Project Name: Electrends

Team Members (Name, GitLab user, EID): 
*   Aimery Methena, ShiftyTheRogue, am87868
*   Dale Kang, dkang13, dmk2489
*   Daniel Dominguez, dandom25, dd32789
*   Kushal Dandamudi, kushalcd, kd22888
*   Swapnil Shaurya, swapnil-shaurya, ss94332

Phase I Git SHA: 7c0696afbff2d446ab7ec105098dbb3bf255549b
Phase II Git SHA: 3d252b64081d364ac25db39e55fa1887a9838254
Phase III Git SHA: c8d576042d780d99dd5bf7ea0fe5aa107ceab350
Phase IV Git SHA: ea5656641dcc7379494a444522080593953ba25f

Final Presentation Video: https://youtu.be/wjUE8cwiWLs

Phase I Project Leader: Aimery Methena
*   Manage everyone's tasks and meetings. Initiate work and discuss with everyone what they would like to do.

Phase II Project Leader: Kushal Dandamudi
*   Manage meetings and deadlines for tasks. Communicate with the TA and memebers accross the team to determine blockers 
    and note progress.

Phase III Project Leader: Swapnil Shaurya
*   Orchestrated meetings and deadlines. Communicated with the team, helped get the project submitted.

Phase IV Project Leader: Daniel Dominguez
*   Helped wrap up the project's final phase.

Gitlab Piplines: https://gitlab.com/dandom25/electrends/-/pipelines

Website: https://www.electrends.me/

Phase I Hours Spent (Estimated, Actual):
*   Aimery Methena: 5, 8
*   Dale Kang: 8, 6
*   Daniel Dominguez: 4, 8
*   Kushal Dandamudi: 4, 7
*   Swapnil Shaurya: 15, 12

Phase II Hours Spent (Estimated, Actual):
*   Aimery Methena: 10, 10
*   Dale Kang: 20, 22
*   Daniel Dominguez: 30, 55
*   Kushal Dandamudi: 20, 50
*   Swapnil Shaurya: 30, 55

Phase III Hours Spent (Estimated, Actual):
*   Aimery Methena: 10, 10
*   Dale Kang: 10/8
*   Daniel Dominguez: 8, 3
*   Kushal Dandamudi: 10, 6
*   Swapnil Shaurya: 10, 6

Phase IV Hours Spent (Estimated, Actual):
*   Aimery Methena: 3, 4
*   Dale Kang: 8, 6
*   Daniel Dominguez: 2, 4
*   Kushal Dandamudi: 6, 8
*   Swapnil Shaurya: 5, 6

Comments:
We used react-bootstrap as a significant workaround for CSS


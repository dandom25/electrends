# pip install selenium
# pip install webdriver_manager

import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options as ChromeOptions
from webdriver_manager.chrome import ChromeDriverManager

# unittest class
class selenium_tests(unittest.TestCase):

    # setup the webdriver to run tests
    def setUp(self):
        service = ChromeService(executable_path=ChromeDriverManager().install())
        options = ChromeOptions()
        self.driver = webdriver.Chrome(options=options, service=service)

    # close the webdriver when finished testing
    def tearDown(self):
        self.driver.quit()
    
    # basic test opening browser
    def test_chrome_0(self):
        self.driver.get("https://www.electrends.me/")
        self.driver.implicitly_wait(3000)

# run the unittests
if __name__ == '__main__':
    unittest.main(warnings='ignore')


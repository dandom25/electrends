import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import About from './pages/About/About.js';
import Home from './pages/Home/Home.js';
import Districts from './pages/Districts/districts.js';
import Politicians from './pages/Politicians/politicians';
import Elections from './pages/PastElections/elections';
import Search from './pages/Search/Search.js';
import {
    BrowserRouter,
    Routes,
    Route,
    Navigate
} from "react-router-dom";
import NavigationBar from './components/Navbar';
import PoliticianPage from './pages/Politicians/politicianPage';
import ElectionPage from './pages/PastElections/electionPage';
import DistrictPage from './pages/Districts/districtPage';
import OurVisualizations from './pages/Visualizations/Ours/ourVisualizations';
import ProviderVisualizations from './pages/Visualizations/Providers/providerVisualizations';


function App() {
    return (
        <>
            <div className="App">
                <NavigationBar></NavigationBar>
                <Routes>
                    <Route exact path='/' element={<Home />} />
                    <Route exact path='/about' element={<About />} />
                    <Route exact path='/politicians' element={<Politicians />} />
                    <Route exact path='/districts' element={<Districts />} />
                    <Route exact path='/elections' element={<Elections />} />
                    <Route exact path='/search' element={<Search />} />
                    <Route exact path='/ourvisualizations' element={<OurVisualizations />} />
                    <Route exact path='/othervisualizations' element={<ProviderVisualizations />}/>
                    <Route path='/politicians/:id' element={<PoliticianPage />} />
                    <Route path='/districts/:id' element={<DistrictPage />} />
                    <Route path='/elections/:id' element={<ElectionPage />} />
                </Routes>
            </div >
        </>
    );
}

export default App;

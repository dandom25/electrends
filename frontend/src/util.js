
// takes in the list of politician objects retrieved from the api and returns
// a list of only the politician names for displaying
export function formatPoliticianArray(objectArray) {
    const result = [];
    objectArray.forEach(element => {
        result.push(element.name);
    });
    return result;
}

// takes in a list of politicians and returns the id of the given name poli
export function findPoliticianID(politicianArray, targetName) {
    let id;
    politicianArray.forEach(politician => {
        if(politician.name === targetName)
            id = politician.id;
    })
    return id;
}

// takes in a twitter profile URL and returns the ID
export function getTwitterID(twitterURL) {
    return twitterURL.substring(twitterURL.lastIndexOf('/') + 1);
}

// takes in a youtube url and makes it come from the embed endpoint
export function makeYoutubeEmbed(youtubeURL) {
    return youtubeURL.replace("watch", "embed");
}
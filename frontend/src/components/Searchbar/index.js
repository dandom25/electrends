import React, { useState } from "react";

const SearchBar = ( props ) => {
    return(
        <div>
            <input
                className="searchbar"
                type="search"
                placeholder="Search here"
                onChange={props.handleQueryChange}
                value={props.query}
            />
        </div>
    )
}

export default SearchBar;
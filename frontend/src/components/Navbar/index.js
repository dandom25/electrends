import React from 'react';
import {
    Navbar,
    Nav,
    Container
} from 'react-bootstrap';
import "./index.css";

const NavigationBar = () => {
    return (
        <Navbar collapseOnSelect className="Navbar-custom" expand="lg" variant="light">
            <Container>
                <Navbar.Brand className="Title-text" href="/">Electrends</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/politicians">Politicians</Nav.Link>
                    <Nav.Link href="/districts">Districts</Nav.Link>
                    <Nav.Link href="/elections">Elections</Nav.Link>
                    <Nav.Link href="/ourvisualizations">Visualizations</Nav.Link>
                    <Nav.Link href="/othervisualizations">Provider Visualizations</Nav.Link>
                    <Nav.Link href="/search">Search</Nav.Link>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default NavigationBar;
import React from "react";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";

const FilterButton = ( props ) => {
    return (
        <UncontrolledDropdown>
            <DropdownToggle caret>{props.display}</DropdownToggle>
            <DropdownMenu>
                <DropdownItem onClick={() => props.click(props.name, undefined)}>None</DropdownItem>
                {props.values?.map(value => (
                    <DropdownItem onClick={() => props.click(props.name, value)}>{value}</DropdownItem>
                ))}
            </DropdownMenu>
        </UncontrolledDropdown>
    )
}

export default FilterButton;
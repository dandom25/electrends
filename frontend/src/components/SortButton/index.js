import React from "react";
import { Button } from "reactstrap";
import { BsArrowUp, BsArrowDown } from "react-icons/bs"

const SortButton = ( props ) => {

    const buttonClick = () => {
        if(props.current === props.name) {
            props.setAscending(!props.isAscending);
        } else {
            props.setAscending(true);
            props.setCurrent(props.name);
        }
    };

    return (
        <Button onClick={buttonClick}>
            {props.display}{props.current === props.name && (props.isAscending ? <BsArrowUp/> : <BsArrowDown/>)}
        </Button>
    )
}

export default SortButton;
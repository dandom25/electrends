import React, { useState } from "react";
import { BsFilter } from "react-icons/bs";
import { Button, Col, Collapse, Row } from "reactstrap";
import FilterButton from "../FilterButton";

const FilterBar = ( props ) => {
    const [showFilters, setShowFilters] = useState(false);

    return(
        <div>
            <Button type="button" onClick={() => setShowFilters(!showFilters)}>Filters <BsFilter/></Button>
            <Collapse isOpen={showFilters}>
                <Row>
                    {props?.filters?.map(filter => (
                        <Col>
                            <FilterButton name={filter.name} display={filter.display} values={filter.values} click={props.click}/>
                        </Col>
                    ))}
                </Row>
            </Collapse>
        </div>
    )
}

export default FilterBar;
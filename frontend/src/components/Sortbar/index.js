import React from "react";
import SortButton from "../SortButton";

const SortBar = ( props ) => {

    return(
        <div>
            {props?.sorts?.map(sort => (
                <SortButton
                    name={sort.name}
                    display={sort.display}
                    current={props.current}
                    setCurrent={props.setCurrent}
                    isAscending={props.isAscending}
                    setAscending={props.setAscending}
                />
            ))}
        </div>
    )
}

export default SortBar;
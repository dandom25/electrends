import React from "react";
import { Button } from "reactstrap";

const Pagination = ({ pages, paginateFunc }) => {
    const pageNumbers = [];

    for(let i = 1; i <= pages; i++) {
        pageNumbers.push(i);
    }

    return(
        <div>
            {pageNumbers.map(number => (
                <Button type="button" onClick={() => paginateFunc(number)} key={number}>{number}</Button>
            ))}
        </div>
                
    )
}

export default Pagination;
import React from "react";
import styles from '../../index.css';
import { Highlight } from "react-highlighter-ts";

function DistrictInstance(props) {

    const linkTo = (route) => {
        window.location.href = route;
    };

    return(
        <tr className="internal_link" onClick={() => linkTo('districts/' + props.id)}>
            <td><Highlight search={props.query}>{props.name}</Highlight></td>
            <td><Highlight search={props.query}>{props.level}</Highlight></td>
            <td><Highlight search={props.query}>{props.population}</Highlight></td>
            <td><Highlight search={props.query}>{props.race}</Highlight></td>
            <td><Highlight search={props.query}>{props.representative}</Highlight></td>
        </tr>
    );
}

export default DistrictInstance;
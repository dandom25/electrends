import React, { useRef, useState, useEffect } from "react";
import axios from 'axios';
import { Card, CardBody, CardImg, CardTitle, CardText, Row, Col, Container, ListGroup, ListGroupItemHeading, ListGroupItem } from "reactstrap";
import { useParams } from "react-router-dom";
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import { findPoliticianID } from "../../util";
import styles from '../../index.css';
// import mapboxgl from "mapbox-gl";


function DistrictPage(props) {

    const districtAPI = 'https://api.electrends.me/districts';

    mapboxgl.accessToken = 'pk.eyJ1IjoiZGthbmcxMyIsImEiOiJjbDFicGNyeHkydXpkM2Nwd2FjMG0wczR2In0.Lu53vWjnFCYEKfuwX3AZjQ';

    const mapContainer = useRef(null);
    const map = useRef(null);

    const { id } = useParams();

    const [districtData, setDistrictData] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {    
        //setLoading(true);
        axios.get(districtAPI + "/" + id)
            .then(function (response) {
                console.log(response);
                setDistrictData(response.data);
                const coordinate = response.data.center_coordinate;
                map.current = new mapboxgl.Map({
                    container: mapContainer.current,
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [coordinate[0], coordinate[1]],
                    zoom: 9
                });
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
    }, [id]);

    const linkTo = (route) => {
        window.location.href = route;
    };

    if (loading) {
        return <h2>Loading...</h2>
    }

    return(
        <Card>
            <CardTitle className="instance_title">{districtData.name}</CardTitle>
            <CardText className="instance_header">Type: {districtData.type}</CardText>
            <Row>
                <Col>
                    <CardTitle className="instance_header">Politicians</CardTitle>
                    <CardBody className="internal_link district_incumbent_card" onClick={() => linkTo('../politicians/' + findPoliticianID(districtData.politicians, districtData.incumbent_name))}>
                        <CardText>Incumbent Representative: {districtData.incumbent_name} ({districtData.incumbent_party})</CardText>
                        <img className="district_incumbent_image" src={districtData.incumbent_pic}/>
                    </CardBody>
                </Col>
                <Col>
                    <CardBody>
                        <CardTitle className="instance_header">Demographics</CardTitle>
                        <CardText>Majority Race: {districtData.majority_race}</CardText>
                        <CardText>Population: {districtData.population}</CardText>
                        <CardText>{districtData.male}% Male, {districtData.female}% Female</CardText>
                        <CardText>Median Income: ${districtData.median_income}</CardText>
                        <CardText>{districtData.hs_grad}% graduated high school, {districtData.college_grad}% graduated college</CardText>
                        <ListGroup>
                            <ListGroupItemHeading>Elections in this district</ListGroupItemHeading>
                            {districtData.elections?.map(elec => (
                                <ListGroupItem className="internal_link" onClick={() => linkTo('../elections/' + elec.id)}>{elec.name}</ListGroupItem>
                            ))}
                        </ListGroup>
                    </CardBody>
                </Col>
            </Row>
            <Row>
                <CardTitle className="instance_header">Other politicians</CardTitle>
                {districtData.politicians?.map(poli => (
                    poli.name !== districtData.incumbent_name &&
                    <Col>
                        <CardBody className="internal_link" onClick={() => linkTo('../politicians/' + poli.id)}>
                            <CardText>{poli.name}</CardText>
                            <CardImg top className="district_other_image" src={poli.img_url} alt="Politician Image not found"/>
                        </CardBody>
                    </Col>
                ))}
            </Row>
            <div ref={mapContainer} className="map-container" />
        </Card>
    );
}

export default DistrictPage;
import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Table, Row, Col } from 'reactstrap';
import DistrictInstance from './districtInstance';
import Pagination from "../../components/Pagination";
import SortBar from "../../components/Sortbar";
import FilterBar from "../../components/Filterbar";
import SearchBar from "../../components/Searchbar";
import { DISTRICT_FILTERS, DISTRICT_RANGE_TO_LIST, DISTRICT_SORTS } from "../../constants";

function Districts() {

    const districtAPI = 'https://api.electrends.me/districts';
    const perPage = 17;

    const [districtData, setDistrictData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [query, setQuery] = useState(null);
    const [govLevelFilter, setGovLevelFilter] = useState(null);
    const [incumbentFilter, setIncumbentFilter] = useState(null);
    const [districtFilter, setDistrictFilter] = useState(null);
    const [raceFilter, setRaceFilter] = useState(null);
    const [currentSort, setCurrentSort] = useState(null);
    const [isSortAscending, setSortAscending] = useState(true);
    const [desc, setDesc] = useState(null);

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        if(isSortAscending) {
            setDesc(null)
        } else {
            setDesc(true)
        }
        axios.get(districtAPI,{
            params: {
                page_num: currentPage,
                per_page: perPage,
                search: query,
                type: govLevelFilter,
                incumbent_party: incumbentFilter,
                number: DISTRICT_RANGE_TO_LIST[districtFilter],
                majority_race: raceFilter,
                sort: currentSort,
                desc: desc
            }})
            .then(function (response) {
                if (isSubscribed) {
                    setDistrictData(response.data);
                }
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                console.log('set to false');
                setLoading(false);
            })
        return () => isSubscribed = false
    }, [currentPage, query, govLevelFilter, incumbentFilter, districtFilter, raceFilter, currentSort, isSortAscending, desc]);

    const paginateFunc = (pageNumber) => setCurrentPage(pageNumber);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    const filterClick = (name, value) => {
        if(name === "type") {
            setGovLevelFilter(value);
        } else if(name === "incumbent_party") {
            setIncumbentFilter(value);
        } else if(name === "number") {
            setDistrictFilter(value);
        } else if(name === "majority_race") {
            setRaceFilter(value);
        }
    }

    return (
        <div className="districts-main">
            <header className="model_title">
                <p>
                    Districts
                </p>
            </header>
            <Row>
                <Col>
                    <FilterBar filters={DISTRICT_FILTERS} click={filterClick}/>
                </Col>
                <Col>
                    <Row>
                        <SearchBar
                            handleQueryChange={handleQueryChange}
                            query={query}
                        />
                    </Row>
                    <Row>
                        <SortBar
                            sorts={DISTRICT_SORTS}
                            current={currentSort}
                            setCurrent={setCurrentSort}
                            isAscending={isSortAscending}
                            setAscending={setSortAscending}
                        />
                    </Row>
                </Col>
            </Row>
            <div>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Table striped>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Population</th>
                                <th>Majority Race</th>
                                <th>Current Representative</th>
                            </tr>
                        </thead>
                        {districtData.districts?.map(district => (
                            <tbody key={district.id}>
                                <DistrictInstance
                                    query={query}
                                    name={district.name}
                                    level={district.type}
                                    population={district.population}
                                    race={district.majority_race}
                                    representative={district.incumbent_name}
                                    id={district.id}
                                />
                            </tbody>
                        ))}
                    </Table>
                )}
            </div>
            <Pagination
                pages={Math.ceil(districtData.meta?.total_count / perPage)}
                paginateFunc={paginateFunc}
            />
        </div>
    );
}
export default Districts;

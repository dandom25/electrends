import React from "react";
import styles from '../../index.css';
import { Highlight } from "react-highlighter-ts";

function ElectionInstance(props) {

    const linkTo = (route) => {
        window.location.href = route;
    };

    return(
        <tr className="internal_link" onClick={() => linkTo('elections/' + props.id)}>
            <th scope="row"><Highlight search={props.query}>{props.year}</Highlight></th>
            <td><Highlight search={props.query}>{props.level}</Highlight></td>
            <td><Highlight search={props.query}>{new Intl.ListFormat('en-GB', { style: 'short', type: 'unit' }).format(props.participants)}</Highlight></td>
            <td><Highlight search={props.query}>{props.turnout}</Highlight></td>
            <td><Highlight search={props.query}>{props.votes}</Highlight></td>
        </tr>
    );
}

export default ElectionInstance;
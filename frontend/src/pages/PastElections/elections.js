import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Col, Row, Table } from 'reactstrap';
import ElectionInstance from './electionInstance';
import Pagination from "../../components/Pagination";
import FilterBar from "../../components/Filterbar";
import SortBar from "../../components/Sortbar";
import SearchBar from "../../components/Searchbar";
import { formatPoliticianArray } from "../../util";
import { DISTRICT_RANGE_TO_LIST, ELECTION_FILTERS, ELECTION_SORTS } from "../../constants";

function Elections() {

    const electionAPI = 'https://api.electrends.me/elections';
    const perPage = 17;

    const [electionData, setElectionData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [query, setQuery] = useState(null);
    const [govLevelFilter, setGovLevelFilter] = useState(null);
    const [yearFilter, setYearFilter] = useState(null);
    const [districtFilter, setDistrictFilter] = useState(null);
    const [partyFilter, setPartyFilter] = useState(null);
    const [currentSort, setCurrentSort] = useState(null);
    const [isSortAscending, setSortAscending] = useState(true);
    const [desc, setDesc] = useState(null);

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        if(isSortAscending) {
            setDesc(null)
        } else {
            setDesc(true)
        }
        axios.get(electionAPI,{
            params: {
                page_num: currentPage,
                per_page: perPage,
                search: query,
                gov_level: govLevelFilter,
                year: yearFilter,
                district_num: DISTRICT_RANGE_TO_LIST[districtFilter],
                winning_party: partyFilter,
                sort: currentSort,
                desc: desc
            }})
            .then(function (response) {
                if (isSubscribed) {
                    setElectionData(response.data);
                }
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                console.log('get updated')
                setLoading(false);
            })
        return () => isSubscribed = false
    }, [currentPage, query, govLevelFilter, yearFilter, districtFilter, partyFilter, currentSort, isSortAscending, desc]);

    const paginateFunc = (pageNumber) => setCurrentPage(pageNumber);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    const filterClick = (name, value) => {
        if(name === "gov_level") {
            setGovLevelFilter(value);
        } else if(name === "year") {
            setYearFilter(value);
        } else if(name === "district_num") {
            setDistrictFilter(value);
        } else if(name === "winning_party") {
            setPartyFilter(value);
        }
    }
    
    return (
        <div className="elections-main">
            <header className="model_title">
                Past Elections
            </header>
            <Row>
                <Col>
                    <FilterBar filters={ELECTION_FILTERS} click={filterClick}/>
                </Col>
                <Col>
                    <Row>
                        <SearchBar
                            handleQueryChange={handleQueryChange}
                            query={query}
                        />
                    </Row>
                    <Row>
                        <SortBar
                            sorts={ELECTION_SORTS}
                            current={currentSort}
                            setCurrent={setCurrentSort}
                            isAscending={isSortAscending}
                            setAscending={setSortAscending}
                        />
                    </Row>
                </Col>
            </Row>
            <div>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Table striped>
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Level of Government</th>
                                <th>Participating Politicians</th>
                                <th>Turnout</th>
                                <th>Total Votes</th>
                            </tr>
                        </thead>
                        {electionData.elections?.map(election => (
                            <tbody key={election.id}>
                                <ElectionInstance
                                    query={query}
                                    year={election.general_date}
                                    level={election.gov_level}
                                    participants={formatPoliticianArray(election.politicians)}
                                    turnout={election.num_votes === -1 ? "No data" : ((election.num_votes / election.district.population) * 100).toFixed(2) + "%"}
                                    votes={election.num_votes === -1 ? "No data" : election.num_votes}
                                    id={election.id}
                                />
                            </tbody>
                        ))}
                    </Table>
                )}
            </div>
            <Pagination
                pages={Math.ceil(electionData.meta?.total_count / perPage)}
                paginateFunc={paginateFunc}
            />
        </div>
    );
}
export default Elections;

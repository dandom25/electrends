import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Card, CardBody, CardImg, CardTitle, CardText, Row, Col, Container } from "reactstrap";
import { useParams } from "react-router-dom";
import styles from '../../index.css';
import { makeYoutubeEmbed } from "../../util";
import ReactPlayer from "react-player"

function ElectionPage(props) {

    const electionAPI = 'https://api.electrends.me/elections';

    const { id } = useParams();

    const [electionData, setElectionData] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios.get(electionAPI + "/" + id)
            .then(function (response) {
                console.log(response);
                setElectionData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
    }, [id]);
    
    const linkTo = (route) => {
        window.location.href = route;
    };

    if (loading) {
        return <h2>Loading...</h2>
    }

    return(
        <Card>
            <CardTitle className="instance_title">{electionData.name}</CardTitle>
            <Row>
                <Col>
                    <CardBody>
                        <CardTitle className="instance_header">District</CardTitle>
                        <CardText className="internal_link" onClick={() => linkTo('../districts/' + electionData.district.id)}>{electionData.district?.name}</CardText>
                    </CardBody>
                    <CardBody>
                        <CardText>Date: {electionData.general_date}</CardText>
                        <CardText>Level: {electionData.gov_level}</CardText>
                        <CardText>Total Votes: {electionData.num_votes === -1 ? "Unknown" : electionData.num_votes}</CardText>
                        <CardText>Winner: {electionData.winning_candidate}</CardText>
                        <CardText>Winner Party: {electionData.winning_party}</CardText>
                    </CardBody>
                </Col>
                <Col>
                    <CardBody>
                        {electionData.youtube_vid &&
                        <div className="video-responsive">
                            <ReactPlayer url={electionData.youtube_vid} width="90%" height="500px" />
                        </div>}
                    </CardBody>
                </Col>
            </Row>
            <Row>
                <CardBody>
                    <CardTitle className="instance_header">Participant politicians</CardTitle>
                    <Container>
                        <Row>
                            {electionData.politicians?.map(poli => (
                                <Col>
                                    <CardBody className="internal_link" onClick={() => linkTo('../politicians/' + poli.id)}>
                                        <CardText>{poli.name}</CardText>
                                        <CardImg top className="election_image" src={poli.img_url} alt="Politician Image not found"/>
                                    </CardBody>
                                </Col>
                            ))}
                        </Row>
                    </Container>
                </CardBody>
            </Row>
        </Card>
    );
}

export default ElectionPage;
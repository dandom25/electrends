import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import React from 'react';


function MainNavbar() {
  return (
    <Navbar collapseOnSelect className="Navbar-custom" expand="lg" variant="dark">
      <Container>
      <Navbar.Brand className="Title-text" href="/home">Electrends</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav.Link href="/About">About</Nav.Link>
        <Nav.Link href="/Home">Home</Nav.Link>
      </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default MainNavbar;

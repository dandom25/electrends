import React from 'react';
import splashpic from "./NewSplashPic.jpg";
import districticon from "./DistrictsIcon.png";
import electionicon from "./ElectionsIcon.png";
import politicianicon from "./PoliticiansIcon.png";
import ReactPlayer from "react-player"
import { Container, Card, Row, Col } from "react-bootstrap";

function Home() {

    const linkTo = (route) => {
        window.location.href = route;
    };

    return (
        <>
        <div
            style={{
                backgroundImage: `url(${splashpic})`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                height: "100vh",
                width: "100vw",
            }}
        > </div>
        <h4><br></br></h4>
        <h3>Learn more about political trends in Texas</h3>
        <Row className="g-3 m-0 justify-content-center" xs="auto" >
            <Col key = "Politicians" as="div" xs="auto" mx-auto>
            <Card onClick={() => linkTo('politicians/')} style={{width: '16rem', cursor: "pointer"}}>
                <Card.Body>
                    <Card.Img variant="top" src = {politicianicon}></Card.Img>
                    <Card.Title>Politicians</Card.Title>
                    <Card.Text>Meet your politicians and representives across Texas.</Card.Text>
                </Card.Body>
            </Card>
            </Col>
            <Col key = "Districts" as="div" xs="auto" mx-auto>
            <Card onClick={() => linkTo('districts/')} style={{width: '16rem', cursor: "pointer"}}>
                <Card.Body>
                    <Card.Img variant="top" src = {districticon}></Card.Img>
                    <Card.Title>Districts</Card.Title>
                    <Card.Text>Discover various electoral districts throughout Texas.</Card.Text>
                </Card.Body>
            </Card>
            </Col>
            <Col key = "Elections" as="div" xs="auto" mx-auto>
            <Card onClick={() => linkTo('elections/')} style={{width: '16rem', cursor: "pointer"}}>
                <Card.Body>
                    <Card.Img variant="top" src = {electionicon}></Card.Img>
                    <Card.Title>Elections</Card.Title>
                    <Card.Text>Learn more about the elections within Texas since 2018.</Card.Text>
                </Card.Body>
            </Card>
            </Col>
        </Row>
        <h4><br></br></h4>
        <h3>Demo Video</h3>
        <Row className="justify-content-center" xs="auto" >
        <ReactPlayer url={"https://youtu.be/wjUE8cwiWLs"} width="90%" height="600px" controls={true} /></Row>
        <h1><br></br></h1>
    </>
    );
}
export default Home;

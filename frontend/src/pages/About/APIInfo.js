import GoogleDevLogo from "./Tool_Pics/GoogleDevLogo.png"
import BallotpediaLogo from "./Tool_Pics/BallotpediaLogo.png"
import USCensusLogo from "./Tool_Pics/USCensusLogo.png"
import CiceroLogo from "./Tool_Pics/CiceroLogo.jpg"
import MapboxLogo from "./Tool_Pics/MapboxLogo.png"

const APIInfo = [
    {
        name: "Google Civic Info API",
        image: GoogleDevLogo,
		description: "Used to find information on elected representatives",
        link: "https://developers.google.com/civic-information",
    },
    {
        name: "Ballotpedia API",
        image: BallotpediaLogo,
		description: "Used to scrape data on politicians, districts and elections",
        link: "https://ballotpedia.org/Main_Page",
    },
    {
        name: "US Census APIs",
        image: USCensusLogo,
		description: "Used to find information on various demographic features of districts",
        link: "https://www.census.gov/data/developers/data-sets.html",
    },
    {
        name: "Cicero API",
        image: CiceroLogo,
		description: "Used to find legislative districts and elected official contact information",
        link: "https://www.programmableweb.com/api/cicero",
    },
    {
		name: "Mapbox GL JS",
        image: MapboxLogo,
		description: "Used to visualize Texas district shapes and locations",
		link: "https://docs.mapbox.com/mapbox-gl-js/api/",
	}
]

export default APIInfo;
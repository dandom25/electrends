import React, { useEffect, useState } from 'react'
import {Row, Col, Card, ListGroup, ListGroupItem} from 'react-bootstrap'
import teamInfo from "./TeamInfo.js"
import toolInfo from "./ToolInfo.js"
import APIInfo from "./APIInfo.js"
import GitLabLogo from "./Tool_Pics/GitLabLogo.png"
import PostmanLogo from "./Tool_Pics/PostmanLogo.png"


const getGitLabInfo = async () => {
    let totalCommitCount = 0, totalIssueCount = 0, totalTestCount = 41;
    teamInfo.forEach((member) => {
        member.issues = 0;
        member.commits = 0;
    });

    let commits = await fetch("https://gitlab.com/api/v4/projects/33830752/repository/commits?per_page=500")
    commits = await commits.json()
    totalCommitCount = commits.length

    commits.forEach((commit) => {
        const{ author_name, author_email } = commit
        teamInfo.forEach((member) => {
            if(member.name === author_name || author_name === member.username || member.username === author_name || member.email === author_email){
                member.commits += 1
            }
        })
    })

    let issues = await fetch("https://gitlab.com/api/v4/projects/33830752/issues?per_page=500")
    issues = await issues.json()
    totalIssueCount = issues.length

    issues.forEach((issue) => {
        const{ closed_by } = issue
        teamInfo.forEach((member) => {
            if (issue.closed_by){
                if(member.name === closed_by.name || closed_by.name === member.username || member.username === closed_by.username || member.name == closed_by.username){
                    member.issues += 1
                }
            }
            
        })
    })

    return {
		totalCommits: totalCommitCount,
		totalIssues: totalIssueCount,
        totalTests: totalTestCount,
        teamMembers: teamInfo,
	}
}

const About = () => {
    const [teamList, setTeamList] = useState([])
	const [totalCommits, setTotalCommits] = useState(0)
	const [totalIssues, setTotalIssues] = useState(0)
	const [totalTests, setTotalTests] = useState(0)

	useEffect(() => {
		const fetchData = async () => {
			if (teamList === undefined || teamList.length === 0) {
				const gitlabInfo = await getGitLabInfo()
				setTotalCommits(gitlabInfo.totalCommits)
				setTotalIssues(gitlabInfo.totalIssues)
				setTotalTests(gitlabInfo.totalTests)
			}
		}
		fetchData()
	}, [teamList])

    const linkTo = (route) => {
        window.open(route);
    };
    
    return (
        <div className="About"> 
            <header className = "About-header">
                <h1>
                    About
                </h1>
            </header>
            <header className = "About-description">
                <h3>
                    Description
                </h3>
                <p>
                    Electrends is a website that allows users to search up information on politicians, past elections, and districts within Texas.<br></br>We aim to help educate users on past and current electoral/political trends throughout Texas.
                </p>
            </header>
            <h4><br></br></h4>
            <header className = "Members">
                <h3>
                    Members
                </h3>
            </header>
            <>
            <Row className="g-3 m-0 justify-content-center" xs="auto">
                {teamInfo.map((member) => {
                    return (
                        <Col key = {member.name} as="div" xs="auto" mx-auto>
                        <Card style={{ width: '17rem' }}>
                            <Card.Body>
                                <Card.Img variant="top" src = {member.image}></Card.Img>
                                <Card.Title>{member.name}</Card.Title>
                                <Card.Subtitle>{member.role}</Card.Subtitle>
                                <Card.Text>{member.bio}</Card.Text>
                            </Card.Body>
                            <ListGroup variant="flush">
                                <ListGroupItem>Commits: {member.commits}</ListGroupItem>
                                <ListGroupItem>Issues: {member.issues}</ListGroupItem>
                                <ListGroupItem>Tests: {member.tests}</ListGroupItem>
                            </ListGroup>
                        </Card>
                        </Col>
                    )
                })}
                </Row>
            </>
            <h1><br></br></h1>
            <header className = "Gitlab Stats">
                <h3>
                    Repository Statistics
                </h3>
            </header>
            <>
            <Row className="g-3 m-0 justify-content-center" xs="auto" >
                    <Col key = "totalCommits" as="div" xs="auto" mx-auto>
                    <Card style={{ width: '16rem' }}>
                        <Card.Body>
                            <Card.Title>Total commits:</Card.Title>
                            <Card.Text>{totalCommits}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                    <Col key = "totalIssues" as="div" xs="auto" mx-auto>
                    <Card style={{ width: '16rem' }}>
                        <Card.Body>
                            <Card.Title>Total issues:</Card.Title>
                            <Card.Text>{totalIssues}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                    <Col key = "totalTests" as="div" xs="auto" mx-auto>
                    <Card style={{ width: '16rem' }}>
                        <Card.Body>
                            <Card.Title>Total tests:</Card.Title>
                            <Card.Text>{totalTests}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                </Row>
            </>
            <h1><br></br></h1>
            <header className = "Tools">
                <h3>
                    Tools Utilized
                </h3>
            </header>
            <>
            <Row className="g-3 m-0 justify-content-center" xs="auto" >
                {toolInfo.map((member) => {
                    return (
                        <Col key = {member.name} as="div" xs="auto" mx-auto>
                            <Card onClick={() => linkTo(member.link)} style={{width: '16rem', height: '340px', cursor: "pointer"}}>
                                <Card.Body>
                                    <Card.Img variant="top" src = {member.image}></Card.Img>
                                    <Card.Title>{member.name}</Card.Title>
                                    <Card.Text>{member.description}</Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })}
                </Row>
            </>
            <h1><br></br></h1>
            <header className = "APIs">
                <h3>
                    APIs Utilized
                </h3>
            </header>
            <>
            <Row className="g-3 m-0 justify-content-center" xs="auto" >
                {APIInfo.map((member) => {
                    return (
                        <Col key = {member.name} as="div" xs="auto" mx-auto>
                            <Card onClick={() => linkTo(member.link)} style={{width: '16rem', height: '355px', cursor: "pointer"}}>
                                <Card.Body>
                                    <Card.Img variant="top" src = {member.image}></Card.Img>
                                    <Card.Title>{member.name}</Card.Title>
                                    <Card.Text>{member.description}</Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })}
                </Row>
            </>
            <h1><br></br></h1>
            <header className = "APIs">
                <h3>
                    Our Work
                </h3>
            </header>
            <>
            <Row className="g-3 m-0 justify-content-center" xs="auto" >
                <Col key = {"GitLab"} as="div" xs="auto" mx-auto>
                    <Card onClick={() => linkTo("https://gitlab.com/dandom25/electrends")} style={{width: '17rem', cursor: "pointer"}}>
                        <Card.Body>
                            <Card.Img variant="top" src = {GitLabLogo}></Card.Img>
                            <Card.Title>GitLab Repository</Card.Title>
                        </Card.Body>
                    </Card>
                </Col>
                <Col key = {"Postman"} as="div" xs="auto" mx-auto>
                    <Card onClick={() => linkTo("https://documenter.getpostman.com/view/12148983/UVyrUGi3")} style={{width: '17rem', cursor: "pointer"}}>
                        <Card.Body>
                            <Card.Img variant="top" src = {PostmanLogo}></Card.Img>
                            <Card.Title>Postman Documentation</Card.Title>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <br></br>
            </>
        </div>
    )
}

export default About

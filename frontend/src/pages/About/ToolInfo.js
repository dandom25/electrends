import AWSLogo from "./Tool_Pics/AWSLogo.png"
import BlackLogo from "./Tool_Pics/BlackLogo.png"
import DiscordLogo from "./Tool_Pics/DiscordLogo.png"
import DockerLogo from "./Tool_Pics/DockerLogo.png"
import FlaskLogo from "./Tool_Pics/FlaskLogo.jpg"
import GitLabLogo from "./Tool_Pics/GitLabLogo.png"
import JestLogo from "./Tool_Pics/JestLogo.png"
import MarshmallowLogo from "./Tool_Pics/MarshmallowLogo.png"
import NamecheapLogo from "./Tool_Pics/NamecheapLogo.png"
import PostgresLogo from "./Tool_Pics/PostgresLogo.png"
import PostmanLogo from "./Tool_Pics/PostmanLogo.png"
import ReactBootstrapLogo from "./Tool_Pics/ReactBootstrapLogo.png"
import ReactLogo from "./Tool_Pics/ReactLogo.png"
import SeleniumLogo from "./Tool_Pics/SeleniumLogo.png"
import SQLAlchemyLogo from "./Tool_Pics/SQLAlchemyLogo.png"

const toolInfo = [
    {
        name: "React",
        image: ReactLogo,
		description: "JavaScript library used for front-end development",
        link: "https://reactjs.org/",
    },
    {
        name: "React Bootstrap",
        image: ReactBootstrapLogo,
		description: "Framework used for front-end UI development",
        link: "https://react-bootstrap.github.io/",
    },
    {
        name: "Flask",
        image: FlaskLogo,
		description: "Framework used for API development",
        link: "https://flask.palletsprojects.com/en/2.0.x/",
    },
    {
        name: "SQLAlchemy",
        image: SQLAlchemyLogo,
		description: "Toolkit for SQL and object-relational mapping",
        link: "https://www.sqlalchemy.org/",
    },
    {
		name: "Marshmallow",
        image: MarshmallowLogo,
		description: "Library for datatype conversion",
		link: "https://marshmallow.readthedocs.io/en/stable/",
	},
    {
		name: "PostgreSQL",
        image: PostgresLogo,
		description: "Relational database management system",
		link: "https://www.postgresql.org/",
	},
    {
        name: "Postman",
        image: PostmanLogo,
		description: "Platform for building and using APIs",
        link: "https://www.postman.com/",
    },
    {
		name: "Black",
        image: BlackLogo,
		description: "Python code formatter",
		link: "https://github.com/psf/black",
	},
	{
		name: "Jest",
        image: JestLogo,
		description: "JavaScript testing framework",
		link: "https://jestjs.io/",
	},
	{
		name: "Selenium",
        image: SeleniumLogo,
		description: "End-to-end UI testing framework",
		link: "https://www.selenium.dev/",
	},
    {
		name: "Docker",
        image: DockerLogo,
		description: "Containerization tool for runtime environments",
		link: "https://docker.com/",
	},
    {
        name: "Amazon Web Services (AWS)",
        image: AWSLogo,
		description: "Cloud hosting platform",
        link: "https://aws.amazon.com/",
    },
    {
        name: "GitLab",
        image: GitLabLogo,
		description: "Version control, Git repository and CI/CD platform",
        link: "https://gitlab.com/",
    },
    {
        name: "NameCheap",
        image: NamecheapLogo,
		description: "Domain name registrar",
        link: "https://namecheap.com/",
    },
    {
        name: "Discord",
        image: DiscordLogo,
		description: "Team communication platform",
        link: "https://discord.com/",
    },
]

export default toolInfo;
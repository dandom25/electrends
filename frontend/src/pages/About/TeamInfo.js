import aimery from "./Team_Pics/Aimery.jpg"
import dale from "./Team_Pics/Dale.jpg"
import daniel from "./Team_Pics/Daniel.jpg"
import kushal from "./Team_Pics/Kushal.jpg"
import swapnil from "./Team_Pics/Swapnil.jpg"

const teamInfo = [
    {
        name: "Aimery Methena",
		username: "ShiftyTheRogue",
		email: "aimery.methena@gmail.com",
		image: aimery,
		role: "Frontend (Phase 1 Leader)",
		bio:
			"I am a Junior studying Computer Science at UT Austin. I was born in Philadelphia, but grew up in The Woodlands, TX. I am both the game ambassador and a competitor for Longhorn Gaming's DotA 2 community.",
		linkedin: "",
		commits: 0,
		issues: 0,
		tests: 0,
    },
    {
        name: "Dale Kang",
		username: "dkang13",
		email: "dkang13@utexas.edu",
		image: dale,
		role: "Frontend",
		bio:
			"I am a Senior studying Computer Science at the University of Texas at Austin. I'm originally from Houston, TX. In my free time, I enjoy listening to music, and playing casual sports and video games.",
		linkedin: "",
		commits: 0,
		issues: 0,
		tests: 20,
    },
    {
        name: "Daniel Dominguez Arroyo",
		username: "dandom25",
		email: "d_dominguez15@yahoo.com",
		image: daniel,
		role: "Backend",
		bio:
			"I am a Junior studying Computer Science at UT Austin. I'm from Austin, so attending university in my hometown is something I looked forward. I like to play basketball with my friends and skate around my neighborhood.",
		linkedin: "",
		commits: 0,
		issues: 0,
		tests: 6,
    },
    {
        name: "Kushal Dandamudi",
		username: "kushalcd",
		email: "kushalcd@gmail.com",
		image: kushal,
		role: "Backend (Phase 2 Leader)",
		bio:
			"I am a Junior studying Computer Science and Math at UT Austin. I'm from Houston, Texas and in my free time I play sports with friends, watch tv shows and play video games. I am also on a competitive dance team at UT.",
		linkedin: "https://www.linkedin.com/in/kushaldandamudi/",
		commits: 0,
		issues: 0,
		tests: 0,
    },
    {
        name: "Swapnil Shaurya",
		username: "swapnil-shaurya",
		email: "swapnil.shaurya@utexas.edu",
		image: swapnil,
		role: "Fullstack",
		bio:
			"I am a Sophomore studying Computer Science and Economics at UT Austin. I'm from Houston, Texas and in my free time I enjoy watching tv shows, cooking, and playing tennis or volleyball with friends.",
		linkedin: "https://www.linkedin.com/in/swapnil-shaurya/",
		commits: 0,
		issues: 0,
		tests: 15,
    }
]

export default teamInfo;
import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Table, Row, Col, Container } from 'reactstrap';
import DistrictInstance from '../Districts/districtInstance';
import PoliticianCard from '../Politicians/politicianCard';
import ElectionInstance from '../PastElections/electionInstance';
import { formatPoliticianArray } from "../../util";


function Districts() {

    const politicianAPI = 'https://api.electrends.me/politicians';
    const districtAPI = 'https://api.electrends.me/districts';
    const electionAPI = 'https://api.electrends.me/elections';
    const rows = 3;
    const cols = 6;
    const perPage = 17;

    const [districtData, setDistrictData] = useState([]);
    const [politicianData, setPoliticianData] = useState([]);
    const [electionData, setElectionData] = useState([]);
    const [query, setQuery] = useState(null);
    const [loading, setLoading] = useState(false);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        axios.get(politicianAPI,{
            params: {
                per_page: perPage,
                search: query
            }
        })
        .then(function (response) {
            if (isSubscribed) {
                setPoliticianData(response.data);
            }
        })
        .catch(function (error) {
            console.error(error);
        })
        .then(function () {
            setLoading(false);
        })
        return () => isSubscribed = false
    }, [query]);

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        axios.get(districtAPI, {
            params: {
                per_page: perPage,
                search: query
            }
        })
        .then(function (response) {
            if (isSubscribed) {
                console.log('set district data')
                setDistrictData(response.data);
            }
        })
        .catch(function (error) {
            console.error(error);
        })
        .then(function () {
            setLoading(false);
        })
        return () => isSubscribed = false
    }, [query]);

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        axios.get(electionAPI,{
            params: {
                per_page: perPage,
                search: query
            }
        })
        .then(function (response) {
            if (isSubscribed) {
                setElectionData(response.data);
            }
        })
        .catch(function (error) {
            console.error(error);
        })
        .then(function () {
            setLoading(false);
        })
        return () => isSubscribed = false
    }, [query]);

    const grid = [];
    for(let i = 0; i < rows; i++) {
        grid.push(politicianData.politicians?.slice(i * cols, (i + 1) * cols));
    }

    return (
        <div>
            <input
                type="search"
                placeholder="Search here"
                onChange={handleQueryChange}
                value={query}
            />
            <div className="politicians-main">
                <header className="model_title">
                    <p>
                        Politicians
                    </p>
                </header>
                <div>
                    { loading ? (
                        <h2>Loading...</h2>
                    ) : (
                        <Container fluid>
                            {grid.map((row) => (
                                <Row>
                                    {row?.map(politician => (
                                        <Col key={politician.id}>
                                            <PoliticianCard
                                                query = {query}
                                                name={politician.name}
                                                years={politician.years_in_office}
                                                level={politician.gov_level}
                                                party={politician.party}
                                                district={politician.district.name}
                                                id={politician.id}
                                                image={politician.img_url}
                                            />
                                        </Col>
                                    ))}
                                </Row>
                            ))}
                        </Container>
                    )}
                </div>
            </div>
            <div className="districts-main">
                <header className="model_title">
                    <p>
                        Districts
                    </p>
                </header>
                <div>
                    { loading ? (
                        <h2>Loading...</h2>
                    ) : (
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Level</th>
                                    <th>Population</th>
                                    <th>Majority Race</th>
                                    <th>Current Representative</th>
                                </tr>
                            </thead>
                            {districtData.districts?.map(district => (
                                <tbody key={district.id}>
                                    <DistrictInstance
                                        query={query}
                                        name={district.name}
                                        level={district.type}
                                        population={district.population}
                                        race={district.majority_race}
                                        representative={district.incumbent_name}
                                        id={district.id}
                                    />
                                </tbody>
                            ))}
                        </Table>
                    )}
                </div>
            </div>
            <div className="elections-main">
                <header className="model_title">
                    Past Elections
                </header>
                <div>
                    { loading ? (
                        <h2>Loading...</h2>
                    ) : (
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>Year</th>
                                    <th>Level of Government</th>
                                    <th>Participating Politicians</th>
                                    <th>Turnout</th>
                                    <th>Total Votes</th>
                                </tr>
                            </thead>
                            {electionData.elections?.map(election => (
                                <tbody key={election.id}>
                                    <ElectionInstance
                                        query={query}
                                        year={election.general_date}
                                        level={election.gov_level}
                                        participants={formatPoliticianArray(election.politicians)}
                                        turnout={election.num_votes === -1 ? "No data" : ((election.num_votes / election.district.population) * 100).toFixed(2) + "%"}
                                        votes={election.num_votes === -1 ? "No data" : election.num_votes}
                                        id={election.id}
                                    />
                                </tbody>
                            ))}
                        </Table>
                    )}
                </div>
            </div>
        </div>
    );
}
export default Districts;

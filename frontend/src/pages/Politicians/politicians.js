import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container } from 'reactstrap';
import PoliticianCard from './politicianCard';
import Pagination from "../../components/Pagination";
import FilterBar from "../../components/Filterbar";
import SearchBar from "../../components/Searchbar";
import { DISTRICT_RANGE_TO_LIST, POLITICIAN_FILTERS, POLITICIAN_SORTS } from "../../constants";
import SortBar from "../../components/Sortbar";

function Politicians() {

    const politicianAPI = 'https://api.electrends.me/politicians';
    const rows = 3;
    const cols = 6;

    const [politicianData, setPoliticianData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [query, setQuery] = useState(null);
    const [govLevelFilter, setGovLevelFilter] = useState(null);
    const [incumbentFilter, setIncumbentFilter] = useState(null);
    const [districtFilter, setDistrictFilter] = useState(null);
    const [partyFilter, setPartyFilter] = useState(null);
    const [currentSort, setCurrentSort] = useState(null);
    const [isSortAscending, setSortAscending] = useState(true);
    const [desc, setDesc] = useState(null);

    useEffect(() => {
        setLoading(true);
        let isSubscribed = true
        console.log(isSortAscending)
        if(isSortAscending) {
            setDesc(null)
        } else {
            setDesc(true)
        }
        axios.get(politicianAPI,{
            params: {
                page_num: currentPage,
                per_page: rows * cols,
                search: query,
                gov_level: govLevelFilter,
                incumbent: incumbentFilter,
                district_num: DISTRICT_RANGE_TO_LIST[districtFilter],
                party: partyFilter,
                sort: currentSort,
                desc: desc
            }})
            .then(function (response) {
                if (isSubscribed) {
                    setPoliticianData(response.data);
                }
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
        return () => isSubscribed = false
    }, [currentPage, query, govLevelFilter, incumbentFilter, districtFilter, partyFilter, currentSort, isSortAscending, desc]);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    const filterClick = (name, value) => {
        if(name === "gov_level") {
            setGovLevelFilter(value);
        } else if(name === "incumbent") {
            setIncumbentFilter(value);
        } else if(name === "district_num") {
            setDistrictFilter(value);
        } else if(name === "party") {
            setPartyFilter(value);
        }
    }

    const grid = [];
    for(let i = 0; i < rows; i++) {
        grid.push(politicianData.politicians?.slice(i * cols, (i + 1) * cols));
    }

    const paginateFunc = (pageNumber) => setCurrentPage(pageNumber);
    
    return (
        <div className="politicians-main">
            <header className="model_title">
                <p>
                    Politicians
                </p>
            </header>
            <Row>
                <Col>
                    <FilterBar filters={POLITICIAN_FILTERS} click={filterClick}/>
                </Col>
                <Col>
                    <Row>
                        <SearchBar
                            handleQueryChange={handleQueryChange}
                            query={query}
                        />
                    </Row>
                    <Row>
                        <SortBar
                            sorts={POLITICIAN_SORTS}
                            current={currentSort}
                            setCurrent={setCurrentSort}
                            isAscending={isSortAscending}
                            setAscending={setSortAscending}
                        />
                    </Row>
                </Col>
            </Row>
            <div>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Container fluid>
                        {grid.map((row) => (
                            <Row>
                                {row?.map(politician => (
                                    <Col key={politician.id}>
                                        <PoliticianCard
                                            query = {query}
                                            name={politician.name}
                                            years={politician.years_in_office}
                                            level={politician.gov_level}
                                            party={politician.party}
                                            district={politician.district.name}
                                            id={politician.id}
                                            image={politician.img_url}
                                        />
                                    </Col>
                                ))}
                            </Row>
                        ))}
                    </Container>
                )}
            </div>
            <Pagination
                pages={Math.ceil(politicianData.meta?.total_count / (rows * cols))}
                paginateFunc={paginateFunc}
            />
        </div>
    );
}
export default Politicians;

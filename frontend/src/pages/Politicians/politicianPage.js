import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Card, CardBody, CardImg, CardTitle, CardText, CardLink, Row, Col } from "reactstrap";
import { useParams } from "react-router-dom";
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import styles from '../../index.css';
import { getTwitterID } from "../../util";

function PoliticianPage(props) {

    const politicianAPI = 'https://api.electrends.me/politicians';

    const { id } = useParams();

    const [politicianData, setPoliticianData] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios.get(politicianAPI + "/" + id)
            .then(function (response) {
                console.log(response);
                setPoliticianData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
    }, [id]);

    const linkTo = (route) => {
        window.location.href = route;
    };

    if (loading) {
        return <h2>Loading...</h2>
    }

    return(
        <Card>
            <CardTitle className="instance_title">{politicianData.name}</CardTitle>
            <Row>
                <Col xs={4}>
                    <img className="politician_page_image" src={politicianData.img_url} alt="Photo not found"/>
                </Col>
                <Col>
                    <CardBody>
                        <CardTitle className="instance_header">Past Elections</CardTitle>
                        {politicianData.elections?.map((election) => (
                            <CardText className="internal_link" onClick={() => linkTo('../elections/' + election.id)}>{election.name}</CardText>
                        ))}
                    </CardBody>
                    <CardBody>
                        <CardTitle className="instance_header">District</CardTitle>
                        <CardText className="internal_link" onClick={() => linkTo('../districts/' + politicianData.district.id)}>{politicianData.district?.name}</CardText>
                    </CardBody>
                    <CardBody>
                        <CardTitle className="instance_header">About</CardTitle>
                        <CardText>Party: {politicianData.party}</CardText>
                        <CardText>Government Level: {politicianData.gov_level}</CardText>
                        <CardText>Incumbent: {politicianData.incumbent ? "Yes" : "No"}</CardText>
                        <CardText>Years in Office: {politicianData.years_in_office}</CardText>
                        <CardLink href={politicianData.facebook}>{politicianData.facebook}</CardLink>
                        
                    </CardBody>
                </Col>
            </Row>
            <Row>
                {politicianData.twitter &&
                <TwitterTimelineEmbed
                    sourceType="profile"
                    screenName={getTwitterID(politicianData.twitter)}
                    options={{
                        height: 1000,
                        margin: "auto"
                    }}
                />}
            </Row>
        </Card>
    );
}

export default PoliticianPage;
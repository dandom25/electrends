import React from "react";
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, } from 'reactstrap';
import styles from "../../index.css"
import { Highlight } from "react-highlighter-ts";

function PoliticianCard(props) {

    // const [name, setName] = useState(props.name);

    // useEffect(() => {
    //     setName(getHighlightedText(props.name, query))
    // }, [query]);

    // // similar to https://stackoverflow.com/questions/29652862/highlight-text-using-reactjs
    // const getHighlightedText = (text, query) => {
    //     // regex for two words: ((query1)|(query2))+
    //     const parts = text.split(new RegExp(`(${query})`, 'gi'));
    //     return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <b>{part}</b> : part)}</span>;
    // }

    const linkTo = (route) => {
        window.location.href = route;
    };

    return(
        <Card className="internal_link politician_card" onClick={() => linkTo('politicians/' + props.id)}>
            <CardImg className="politician_image" top width="100%" src={props.image} alt="Image didn't load"/>
            <CardBody>
                <CardTitle><Highlight search={props.query}>{props.name}</Highlight></CardTitle>
                <CardSubtitle><Highlight search={props.query}>{props.level}</Highlight></CardSubtitle>
                <CardText><Highlight search={props.query}>{props.years}</Highlight>  years in office</CardText>
                <CardText><Highlight search={props.query}>{props.party}</Highlight></CardText>
                <CardText><Highlight search={props.query}>{props.district}</Highlight></CardText>
            </CardBody>
        </Card>
    );
}

export default PoliticianCard;
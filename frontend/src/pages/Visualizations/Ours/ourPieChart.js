import React from "react";
import { PieChart, Pie, Cell } from "recharts";
import { PIE_COLORS } from "../../../constants";
import { Col, Row } from 'reactstrap';

function OurPieChart(props) {

    console.log("piedata", props?.data);

    let renderLabel = function(entry) {
        return entry.name;
    }
    
    return(
        <Row>
            <Col>
            <Row className="justify-content-center">
                <PieChart width={500} height={400}>
                    <Pie data={props?.data[0]} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={150} label={renderLabel}>
                    {props?.data[0]?.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={PIE_COLORS[index]}/>
                    ))}</Pie>
                </PieChart>
                <h5>Households Earning Under $50000</h5>
                </Row>
            </Col>
            <Col>
                <Row className="justify-content-center">
                <PieChart width={500} height={400}>
                    <Pie data={props?.data[1]} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={150} label={renderLabel}>
                    {props?.data[1]?.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={PIE_COLORS[index]}/>
                    ))}</Pie>
                </PieChart>
                <h5>All Households</h5>
                </Row>
            </Col>
        </Row>
    );

}

export default OurPieChart;
import React from "react";
import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from "recharts";
import { Col, Row } from 'reactstrap';

function OurBarChart(props) {

    console.log("bardata", props?.data);
    
    return(
        <Row className="justify-content-center">
        <BarChart width={730} height={250} data={props?.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis unit={"%"}/>
            <Tooltip />
            <Legend />
            <Bar dataKey="Highschool" fill="#8884d8" />
            <Bar dataKey="College" fill="#82ca9d" />
        </BarChart></Row>
    );

}

export default OurBarChart;
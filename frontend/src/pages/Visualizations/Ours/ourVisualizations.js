import axios from "axios";
import React, { useEffect, useState } from "react";
import { POLITICIAN_API, DISTRICT_API, ELECTION_API } from "../../../constants";
import OurBarChart from "./ourBarChart";
import OurPieChart from "./ourPieChart";
import OurScatterChart from "./ourScatterChart";

function OurVisualizations() {

    const income_threshold = 50000;

    const [loading, setLoading] = useState(false);
    const [scatterLoading, setScatterLoading] = useState(false);
    const [pieData, setPieData] = useState([]);
    const [barData, setBarData] = useState([]);
    const [scatterData, setScatterData] = useState([]);

    useEffect(() => {
        setLoading(true);
        axios.get(DISTRICT_API, {
            params: {
                per_page: 1000,
                type: "House of Representatives"
            }})
            .then(function (response) {
                console.log(response);
                setPieData(computePieData(response.data.districts));
                setBarData(computeBarData(response.data.districts));
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
    }, []);

    useEffect(() => {
        setScatterLoading(true);
        axios.get(ELECTION_API, {
            params: {
                per_page: 1000
            }})
            .then(function (response) {
                console.log(response);
                setScatterData(computeScatterData(response.data.elections));
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setScatterLoading(false);
            })
    }, []);

    const computePieData = (districtData) => {
        var low = [ {"name": "Hispanic", "value": 0},
                    {"name": "Asian", "value": 0},
                    {"name": "Black", "value": 0},
                    {"name": "Native or Pacific Islander", "value": 0},
                    {"name": "White", "value": 0}];
        var all = [ {"name": "Hispanic", "value": 0},
                    {"name": "Asian", "value": 0},
                    {"name": "Black", "value": 0},
                    {"name": "Native or Pacific Islander", "value": 0},
                    {"name": "White", "value": 0}];
        let low_cnt = 0;
        let all_cnt = 0;
        districtData.forEach(district => {
            all[0].value += district.ethnicity_hispanic;
            all[1].value += district.race_asian;
            all[2].value += district.race_black;
            all[3].value += district.race_native;
            all[3].value += district.race_pacific_island;
            all[4].value += district.race_white;
            all_cnt++;
            if(district.median_income <= income_threshold) {
                low[0].value += district.ethnicity_hispanic;
                low[1].value += district.race_asian;
                low[2].value += district.race_black;
                low[3].value += district.race_native;
                low[3].value += district.race_pacific_island;
                low[4].value += district.race_white;
                low_cnt++;
            }
        });

        low.forEach(group => {
            group.value = group.value / low_cnt;
        });

        all.forEach(group => {
            group.value = group.value / all_cnt;
        });

        return [low, all];
    }

    const computeBarData = (districtData) => {
        var data = [{"name": "White", "Highschool": 0, "College": 0},
                    {"name": "Black", "Highschool": 0, "College": 0},
                    {"name": "Hispanic", "Highschool": 0, "College": 0},
                    {"name": "Native or Pacific Islander", "Highschool": 0, "College": 0},
                    {"name": "Asian", "Highschool": 0, "College": 0}];
        const cnts = [0, 0, 0, 0, 0];

        districtData.forEach(district => {
            if(district.majority_race === "White"){
                data[0].Highschool += district.hs_grad;
                data[0].College += district.college_grad;
                ++cnts[0];
            } else if(district.majority_race === "Black"){
                data[1].Highschool += district.hs_grad;
                data[1].College += district.college_grad;
                ++cnts[1];
            } else if(district.majority_race === "Hispanic"){
                data[2].Highschool += district.hs_grad;
                data[2].College += district.college_grad;
                ++cnts[2];
            } else if(district.majority_race === "Asian"){
                data[4].Highschool += district.hs_grad;
                data[4].College += district.college_grad;
                ++cnts[4];
            } else {
                data[3].Highschool += district.hs_grad;
                data[3].College += district.college_grad;
                ++cnts[3];
            }
        });

        data.forEach((category, index) => {
            category.College /= cnts[index];
            category.Highschool /= cnts[index];
        });

        return data;
    }

    const computeScatterData = (electionData) => {
        const dems = [];
        const repubs = [];
        const liberts = [];
        const other = [];
        
        electionData.forEach(election => {
            if(election.num_votes > 1 && election.num_votes < 1000000) {
                if(election.winning_party === "Democratic") {
                    dems.push({ "x": election.district.population,/*Date.parse(election.general_date),*/
                                "y": ((election.num_votes / election.district.population) * 100).toFixed(2)})
                } else if(election.winning_party === "Republican") {
                    repubs.push({ "x": election.district.population,/*Date.parse(election.general_date),*/
                                "y": ((election.num_votes / election.district.population) * 100).toFixed(2)})
                } else if(election.winning_party === "Libertarian") {
                    liberts.push({ "x": election.district.population,/*Date.parse(election.general_date),*/
                                "y": ((election.num_votes / election.district.population) * 100).toFixed(2)})
                } else if(election.winning_party !== "TBD"){
                    other.push({ "x": election.district.population,/*Date.parse(election.general_date),*/
                                "y": ((election.num_votes / election.district.population) * 100).toFixed(2)})
                }
            }
        });
        dems.sort((a, b) => (a.x > b.x) ? 1 : -1);
        repubs.sort((a, b) => (a.x > b.x) ? 1 : -1);
        liberts.sort((a, b) => (a.x > b.x) ? 1 : -1);
        other.sort((a, b) => (a.x > b.x) ? 1 : -1);
        return [dems, repubs, liberts, other];
    }

    if(loading || scatterLoading) {
        return(
            <div>
                <h1>Loading</h1>
            </div>
        )
    }

    return(
        <div>
            <h1>Our Visualizations</h1>
            <h1><br></br></h1>
            <h4>Race Demographics within Texas</h4>
            <OurPieChart data={pieData}/>
            <h2><br></br></h2>
            <h4>Education by Majority Race Within Electoral District</h4>
            <OurBarChart data={barData}/>
            <h2><br></br></h2>
            <h4>Election Results based on District Population and Turnout</h4>
            <OurScatterChart data={scatterData}/>
            <h2><br></br></h2>
        </div>
    );

};

export default OurVisualizations;
import React, {PureComponent} from "react";
import { Treemap, Tooltip } from 'recharts';
import { Col, Row } from 'reactstrap';

function ProviderTreeMap(props) {

    console.log("treemapdata", props.data[1]);

    const CustomTooltip = ({ active, payload }) => {
  
        if (active && payload && payload.length) {
          return (
            <div className="custom-tooltip">
              <p>{`${payload[0].payload.name} : $${payload[0].value}`}</p>
            </div>
          );
        }
      
        return null;
      };

    const COLORS = ["#e9141d", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#e9141d", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#e9141d","#0015bc", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#0015bc", "#e9141d", "#0015bc", "#0015bc", "#e9141d", "#0015bc"];
    class CustomizedContent extends PureComponent {
        render() {
          const { root, depth, x, y, width, height, index, payload, colors, rank, name } = this.props;
      
          return (
            <g>
              <rect
                x={x}
                y={y}
                width={width}
                height={height}
                style={{
                  fill: colors[index],
                  stroke: '#fff',
                  strokeWidth: 2 / (depth + 1e-10),
                  strokeOpacity: 1 / (depth + 1e-10),
                }}
              />
              {index <= 7 ? (
                <text x={x + width / 2} y={y + height / 2 + 7} textAnchor="middle" fill="#fff" fontSize={14}>
                  {name}
                </text>
              ) : null}
            </g>
          );
        }
      }
    
    return(
      <Row className="justify-content-center">
        <Treemap width={1000} height={400} data={props?.data[0]} dataKey="size" ratio={4 / 3} stroke="#fff" fill={props?.data[1]} content={<CustomizedContent colors={COLORS} />} >
            <Tooltip content={<CustomTooltip />} wrapperStyle={{ backgroundColor: "white" }}/>
        </Treemap>
      </Row>
    );

}

export default ProviderTreeMap;
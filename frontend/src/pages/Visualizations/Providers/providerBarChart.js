import React, { useEffect, useState } from "react";
import { Col, Row } from 'reactstrap';
import Spinner from 'react-bootstrap/Spinner';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

function ProviderBarChart(props) {

    console.log("bardata------------>", props?.data);

    const [loading, setLoading] = useState(true);
    const barColors = ["purple", "pink", "orange", "brown", "violet","purple", "pink", "orange", "brown", "violet","purple", "pink", "orange"]

    useEffect(() => {
      if(props.data.length !=0){
        setLoading(false);  
      }
  }, [props]);

    if(!loading){
      return (
        <Row className="justify-content-center">
          <BarChart width={1000} height={600} data={props.data}>
              <Bar dataKey="count" fill="green" >
                {
                  props.data.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={barColors[index % 20]} />
                  ))
                }
              </Bar>
              <XAxis dataKey="sector" tick={false}/>
              <YAxis tick={{fontSize: 30}} />
              <Tooltip cursor={{fill: 'transparent'}} />
          </BarChart>
        </Row>
        );
    }
    else{
      return(
        <Row className="justify-content-center" style = {{ marginTop : 50}}>
          <Spinner animation="grow" variant="primary" size="lg"/>
        </Row>
      );
    }

}

export default ProviderBarChart;
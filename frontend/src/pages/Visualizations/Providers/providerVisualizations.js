import axios from "axios";
import React, { useEffect, useState } from "react";
import { CONGRESSPEOPLE_API, CONGRESSPEOPLE_API_ALL, COMMITTEE_API, INDUSTRY_API, states } from "../../../constants";
import ProviderPieChart from "./providerPieChart";
import ProviderTreeMap from "./providerTreeMap";
import ProviderBarChart from "./providerBarChart";

function ProviderVisualizations() {
    
    const [loading, setLoading] = useState(false);
    const [pieData, setPieData] = useState([]);
    const [treeData, setTreeData] = useState([]);
    const [barData, setBarData] = useState([]);

    const commonURLBar = "https://api.catchingcongress.me/industries?page=1&sector=";
    const endPointsBar = ["Agribusiness","Communications%2FElectronics","Construction","Defense","Energy+%26+Natural+Resources","Finance,+Insurance+%26+Real+Estate","Health","Lawyers+%26+Lobbyists","Transportation","Misc+Business","Labor","Ideological%2FSingle-Issue","Other"]
    let tempBarData = [];

    
    useEffect(() => {
        setLoading(true);

        endPointsBar.forEach(element => {
            let url = String(commonURLBar + element);

            axios.get(url)
            .then(function (response) {
                let sectorData = {
                    count: response.data.count,
                    sector: response.data.page[0].sector
                };
                
                tempBarData.push(sectorData);
    
            }).catch(function (error) {
                console.error(error);
            })
        })

        if(tempBarData){
            setBarData(tempBarData);
            setLoading(false);
        }

    }, []);

    var api_responses = []
    CONGRESSPEOPLE_API_ALL.forEach(url => {
        api_responses.push(axios.get(url))
    })    

    useEffect(() => {
        setLoading(true);
        axios.all(api_responses)
            .then(axios.spread((...responses) => {
                var responseData = []
                responses.forEach(entry => {
                    responseData.push(entry.data);
                })
                console.log(responses);
                setPieData(computePieData(responseData));
            }))
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
        }, []);

    useEffect(() => {
        setLoading(true);
        axios.get(
            INDUSTRY_API, 
            {
            params: {
                page: 1,
                sort: "total_contributed",
                desc: true
            }}
            )
            .then(function (response) {
                console.log(response);
                setTreeData(computeTreeData(response.data.page));
            })
            .catch(function (error) {
                console.error(error);
            })
            .then( function () {
                setLoading(false);
            })
    }, []);

    const computeTreeData = (industryData) => {
        const data = [];
        industryData.forEach(industry => {
            data.push({"name": industry.name, "size": industry.total_contributed})
        })
        return [data];
    }

    const computePieData = (peopleData) => {
        var repData = {};
        var demData = {};
        states.forEach(state => {
            repData[state.value] = 0;
            demData[state.value] = 0;
        });
        let rep_cnt = 0;
        let dem_cnt = 0;
        peopleData.forEach(pageResponse => {
            pageResponse.page.forEach(person => {
                if (person.political_party === "R") {
                    repData[person.state] += 1;
                    rep_cnt+= 1;
                } else if (person.political_party === "D") {
                    demData[person.state] += 1;
                    dem_cnt+= 1;
                }
            })
        });
        var repPieData = [];
        var demPieData = [];
        let rep_other = 0;
        let dem_other = 0;
        let min_rep = rep_cnt/25;
        let min_dem = dem_cnt/25;
        for (const [key, value] of Object.entries(repData)) {
            if (value > min_rep) {
                repPieData.push({"name": key, "value": value});
            } else {
                rep_other+= value;
            }
        }
        repPieData.push({"name": "Other", "value": rep_other});
        for (const [key, value] of Object.entries(demData)) {
            if (value > min_dem) {
                demPieData.push({"name": key, "value": value});
            } else {
                dem_other+= value;
            }   
        }
        demPieData.push({"name": "Other", "value": dem_other});

        return [repPieData, demPieData]
    }

    if(loading || loading) {
        return(
            <div>
                <h1>Loading</h1>
            </div>
        )
    }

    return(
        <div>
            <h1>Provider Visualizations</h1>
            <h1><br></br></h1>
            <h4>Congresspeople by State</h4>
            <ProviderPieChart data={pieData}/>
            <h2><br></br></h2>
            <h4>Industries by Amount Contributed</h4>
            <ProviderTreeMap data={treeData}/>
            <h2><br></br></h2>
            <h4>Industries per Sector</h4>
            <ProviderBarChart data={barData}/>
            <h2><br></br></h2>
        </div>
    );

};

export default ProviderVisualizations;
export const POLITICIAN_API = 'https://api.electrends.me/politicians';
export const ELECTION_API = 'https://api.electrends.me/elections';
export const DISTRICT_API = 'https://api.electrends.me/districts';

// provider API endpoints
export const CONGRESSPEOPLE_API = 'https://api.catchingcongress.me/congresspeople';
export const CONGRESSPEOPLE_API_ALL = [CONGRESSPEOPLE_API + "?page=1", CONGRESSPEOPLE_API + "?page=2", CONGRESSPEOPLE_API + "?page=3", 
                                      CONGRESSPEOPLE_API + "?page=4", CONGRESSPEOPLE_API + "?page=5", CONGRESSPEOPLE_API + "?page=6", 
                                      CONGRESSPEOPLE_API + "?page=7", CONGRESSPEOPLE_API + "?page=8", CONGRESSPEOPLE_API + "?page=9", 
                                      CONGRESSPEOPLE_API + "?page=10", CONGRESSPEOPLE_API + "?page=11", CONGRESSPEOPLE_API + "?page=12", 
                                      CONGRESSPEOPLE_API + "?page=13", CONGRESSPEOPLE_API + "?page=14", CONGRESSPEOPLE_API + "?page=15", 
                                      CONGRESSPEOPLE_API + "?page=16", CONGRESSPEOPLE_API + "?page=17", CONGRESSPEOPLE_API + "?page=18", 
                                      CONGRESSPEOPLE_API + "?page=19", CONGRESSPEOPLE_API + "?page=20", CONGRESSPEOPLE_API + "?page=21", 
                                      CONGRESSPEOPLE_API + "?page=22"]

export const COMMITTEE_API = 'https://api.catchingcongress.me/committees';
export const INDUSTRY_API = 'https://api.catchingcongress.me/industries';

// taken from here: https://gist.github.com/mucar/3898821
export const PIE_COLORS = ['#FF6633', '#FFB399', '#FF33FF', '#00B3E6', '#E6B333', 
                          '#3366E6', '#999966', '#B34D4D', '#80B300', '#809900', 
                          '#E6B3B3', '#6680B3', '#66991A', '#FF99E6', '#CCFF1A', 
                          '#FF1A66', '#E6331A', '#33FFCC', '#66994D', '#B366CC', 
                          '#4D8000', '#B33300', '#CC80CC', '#66664D', '#991AFF', 
                          '#E666FF', '#4DB3FF', '#1AB399', '#E666B3', '#33991A', 
                          '#CC9999', '#B3B31A', '#00E680', '#4D8066', '#809980', 
                          '#E6FF80', '#1AFF33', '#999933', '#FF3380', '#CCCC00', 
                          '#66E64D', '#4D80CC', '#9900B3', '#E64D66', '#4DB380',];

export const POLITICIAN_FILTERS = [
    {
        name: "gov_level",
        display: "Government Level",
        values: [
            "Texas State Senate",
            "Texas House of Representatives",
            "Texas Congressional District",
            "US Senate",
            "US Presidency"
        ]
    },
    {
        name: "incumbent",
        display: "Incumbent",
        values: [
            "True",
            "False"
        ]
    },
    {
        name: "district_num",
        display: "District Number",
        values: [
            "0 - 25",
            "26 - 50",
            "51 - 75",
            "76 - 100",
            "101 - 125",
            "126 - 150"
        ]
    },
    {
        name: "party",
        display: "Party",
        values: [
            "Republican",
            "Democrat",
            "Libertarian",
            "Other"
        ]
    },
]

export const POLITICIAN_SORTS = [
    {
        name: "name",
        display: "Name"
    },
    {
        name: "years_in_office",
        display: "Years in Office"
    },
    {
        name: "district_num",
        display: "District Num"
    }
]

export const DISTRICT_FILTERS = [
    {
        name: "type",
        display: "Type",
        values: [
            "State Senate",
            "House of Representatives",
            "Congressional",
            "Presidential",
            "US Senate"
        ]
    },
    {
        name: "incumbent_party",
        display: "Incumbent Party",
        values: [
            "Republican",
            "Democrat",
            "Libertarian",
            "Other"
        ]
    },
    {
        name: "number",
        display: "District Number",
        values: [
            "0 - 25",
            "26 - 50",
            "51 - 75",
            "76 - 100",
            "101 - 125",
            "126 - 150"
        ]
    },
    {
        name: "majority_race",
        display: "Majority Race",
        values: [
            "White",
            "Black",
            "Asian",
            "Native American",
            "Pacific Islander"
        ]
    },
]

export const DISTRICT_SORTS = [
    {
        name: "population",
        display: "Population"
    },
    {
        name: "number",
        display: "District Number"
    },
]

export const ELECTION_FILTERS = [
    {
        name: "gov_level",
        display: "Government Level",
        values: [
            "Texas State Senate",
            "Texas House of Representatives",
            "Texas Congressional District",
            "US Senate",
            "US Presidency"
        ]
    },
    {
        name: "year",
        display: "Election Year",
        values: [
            "2018",
            "2020",
            "2022"
        ]
    },
    {
        name: "district_num",
        display: "District Number",
        values: [
            "0 - 25",
            "26 - 50",
            "51 - 75",
            "76 - 100",
            "101 - 125",
            "126 - 150"
        ]
    },
    {
        name: "winning_party",
        display: "Winning Party",
        values: [
            "Republican",
            "Democrat",
            "Libertarian",
            "Other"
        ]
    },
]

export const ELECTION_SORTS = [
    {
        name: "district_num",
        display: "District Number"
    },
    {
        name: "num_votes",
        display: "Total Votes"
    },
    {
        name: "year",
        display: "Election Year"
    },
]

export const DISTRICT_RANGE_TO_LIST = {
    "0 - 25": "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25",
    "26 - 50": "26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50",
    "51 - 75": "51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75",
    "76 - 100": "76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100",
    "101 - 125": "101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125",
    "126 - 150": "126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150"
}

export const states = [
    {
      name: 'Alabama',
      value: 'AL',
    },
    {
      name: 'Alaska',
      value: 'AK',
    },
    {
      name: 'Arizona',
      value: 'AZ',
    },
    {
      name: 'Arkansas',
      value: 'AR',
    },
    {
      name: 'California',
      value: 'CA',
    },
    {
      name: 'Colorado',
      value: 'CO',
    },
    {
      name: 'Connecticut',
      value: 'CT',
    },
    {
      name: 'Delaware',
      value: 'DE',
    },
    {
      name: 'District of Columbia',
      value: 'DC',
    },
    {
      name: 'Florida',
      value: 'FL',
    },
    {
      name: 'Georgia',
      value: 'GA',
    },
    {
      name: 'Hawaii',
      value: 'HI',
    },
    {
      name: 'Idaho',
      value: 'ID',
    },
    {
      name: 'Illinois',
      value: 'IL',
    },
    {
      name: 'Indiana',
      value: 'IN',
    },
    {
      name: 'Iowa',
      value: 'IA',
    },
    {
      name: 'Kansas',
      value: 'KS',
    },
    {
      name: 'Kentucky',
      value: 'KY',
    },
    {
      name: 'Louisiana',
      value: 'LA',
    },
    {
      name: 'Maine',
      value: 'ME',
    },
    {
      name: 'Montana',
      value: 'MT',
    },
    {
      name: 'Nebraska',
      value: 'NE',
    },
    {
      name: 'Nevada',
      value: 'NV',
    },
    {
      name: 'New Hampshire',
      value: 'NH',
    },
    {
      name: 'New Jersey',
      value: 'NJ',
    },
    {
      name: 'New Mexico',
      value: 'NM',
    },
    {
      name: 'New York',
      value: 'NY',
    },
    {
      name: 'North Carolina',
      value: 'NC',
    },
    {
      name: 'North Dakota',
      value: 'ND',
    },
    {
      name: 'Ohio',
      value: 'OH',
    },
    {
      name: 'Oklahoma',
      value: 'OK',
    },
    {
      name: 'Oregon',
      value: 'OR',
    },
    {
      name: 'Maryland',
      value: 'MD',
    },
    {
      name: 'Massachusetts',
      value: 'MA',
    },
    {
      name: 'Michigan',
      value: 'MI',
    },
    {
      name: 'Minnesota',
      value: 'MN',
    },
    {
      name: 'Mississippi',
      value: 'MS',
    },
    {
      name: 'Missouri',
      value: 'MO',
    },
    {
      name: 'Pennsylvania',
      value: 'PA',
    },
    {
      name: 'Rhode Island',
      value: 'RI',
    },
    {
      name: 'South Carolina',
      value: 'SC',
    },
    {
      name: 'South Dakota',
      value: 'SD',
    },
    {
      name: 'Tennessee',
      value: 'TN',
    },
    {
      name: 'Texas',
      value: 'TX',
    },
    {
      name: 'Utah',
      value: 'UT',
    },
    {
      name: 'Vermont',
      value: 'VT',
    },
    {
      name: 'Virginia',
      value: 'VA',
    },
    {
      name: 'Washington',
      value: 'WA',
    },
    {
      name: 'West Virginia',
      value: 'WV',
    },
    {
      name: 'Wisconsin',
      value: 'WI',
    },
    {
      name: 'Wyoming',
      value: 'WY',
    },
  ];
  

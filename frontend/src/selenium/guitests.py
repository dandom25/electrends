# pip install selenium
# pip install webdriver_manager

import unittest
from time import sleep
from sys import platform
from selenium import webdriver

website = 'https://www.electrends.me'

# unittest class
class selenium_tests(unittest.TestCase):

    # setup the webdriver to run tests
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)


    # close the webdriver when finished testing
    def tearDown(self):
        self.driver.quit()

    # root element
    def test_root_element(self):
        self.driver.get(website)
        sleep(3)
        result = self.driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    # title on home page
    def test_home(self):
        self.driver.get(website)
        sleep(3)
        title = self.driver.find_element_by_tag_name('h3')
        self.assertEqual(title.text, 'Learn more about political trends in Texas')
    
    # title on about page
    def test_about_title(self):
        self.driver.get(f'{website}/about')
        sleep(3)
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'About')

    # cards on about page
    def test_about_cards(self):
        self.driver.get(f'{website}/about')
        sleep(3)
        cards = self.driver.find_elements_by_class_name('card-body')
        self.assertEqual(len(cards), 30)

    # politicians page
    def test_about_politicians(self):
        self.driver.get(f'{website}/politicians/1')
        sleep(3)
        titles = self.driver.find_elements_by_class_name('card-title')
        self.assertEqual(len(titles), 4)

    # politicians page
    def test_about_politicians_2(self):
        self.driver.get(f'{website}/politicians/1')
        sleep(3)
        cards = self.driver.find_elements_by_class_name('card-body')
        self.assertEqual(len(cards), 3)

    # districts page
    def test_about_districts(self):
        self.driver.get(f'{website}/districts/1')
        sleep(3)
        titles = self.driver.find_elements_by_class_name('card-title')
        self.assertEqual(len(titles), 4)

    # districts page
    def test_about_districts_2(self):
        self.driver.get(f'{website}/districts/1')
        sleep(3)
        cards = self.driver.find_elements_by_class_name('card-body')
        self.assertEqual(len(cards), 3)

    # elections page
    def test_about_elections(self):
        self.driver.get(f'{website}/elections/1')
        sleep(3)
        titles = self.driver.find_elements_by_class_name('card-title')
        self.assertEqual(len(titles), 3)

    # elections page
    def test_about_elections_2(self):
        self.driver.get(f'{website}/elections/1')
        sleep(4)
        cards = self.driver.find_elements_by_class_name('card-body')
        self.assertEqual(len(cards), 7)

# run the unittests
if __name__ == '__main__':
    unittest.main(warnings='ignore')

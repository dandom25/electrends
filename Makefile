.DEFAULT_GOAL := all
SHELL         := bash

# All of these make commands must be called in root directory

all:

# auto format the code
format:
	black ./backend/*.py

install:
	pip install -r ./backend/requirements.txt

npm_install:
	cd frontend/
	npm install react-scripts react-bootstrap bootstrap axios react-twitter-embed

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

from init_db import init_db
from os import major, popen
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import urllib
import json
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump

app = Flask(__name__)
CORS(app)
db = init_db(app)
ma = Marshmallow(app)


# following this tutorial given by TA: https://medium.com/@caitlinlien_32145/cs-373-databases-for-dummies-3b82bbfc371
# following https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/ for relationship structure in models
# using https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html as well for additional reference
class District(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    type = db.Column(db.String())
    number = db.Column(db.Integer)
    incumbent_name = db.Column(db.String())
    incumbent_party = db.Column(db.String())
    incumbent_pic = db.Column(db.String())
    population = db.Column(db.Integer)
    race_white = db.Column(db.Float)
    race_black = db.Column(db.Float)
    race_asian = db.Column(db.Float)
    race_native = db.Column(db.Float)
    race_pacific_island = db.Column(db.Float)
    majority_race = db.Column(db.String())
    ethnicity_hispanic = db.Column(db.Float)
    median_income = db.Column(db.Float)
    hs_grad = db.Column(db.Float)
    college_grad = db.Column(db.Float)
    male = db.Column(db.Float)
    female = db.Column(db.Float)
    map_geoJSON = db.Column(db.String())
    min_latitude = db.Column(db.Float)
    max_latitude = db.Column(db.Float)
    min_longitude = db.Column(db.Float)
    max_longitude = db.Column(db.Float)
    border_latitudes = db.Column(db.ARRAY(db.Float))
    border_longitudes = db.Column(db.ARRAY(db.Float))
    center_coordinate = db.Column(db.ARRAY(db.Float))  # latitude, longitude
    politicians = db.relationship(
        "Politician", backref="district", lazy=False
    )  # handles the one to many relationship
    elections = db.relationship(
        "Election", backref="district", lazy=False
    )  # handles the one to many relationship

    def __init__(
        self,
        name,
        type,
        number,
        incumbent_name,
        incumbent_party,
        incumbent_pic,
        population,
        race_white,
        race_black,
        race_asian,
        race_native,
        race_pacific_island,
        median_income,
        hs_grad,
        college_grad,
        male,
        female,
        majority_race,
        ethnicity_hispanic,
        map_geoJSON,
        min_latitude,
        max_latitude,
        min_longitude,
        max_longitude,
        border_latitudes,
        border_longitudes,
        center_coordinate,
    ):
        self.name = name
        self.type = type
        self.number = number
        self.incumbent_name = incumbent_name
        self.incumbent_party = incumbent_party
        self.incumbent_pic = incumbent_pic
        self.population = population
        self.race_white = race_white
        self.race_black = race_black
        self.race_asian = race_asian
        self.race_native = race_native
        self.race_pacific_island = race_pacific_island
        self.median_income = median_income
        self.hs_grad = hs_grad
        self.college_grad = college_grad
        self.male = male
        self.female = female
        self.majority_race = majority_race
        self.ethnicity_hispanic = ethnicity_hispanic
        self.map_geoJSON = map_geoJSON
        self.min_latitude = min_latitude
        self.max_latitude = max_latitude
        self.min_longitude = min_longitude
        self.max_longitude = max_longitude
        self.border_latitudes = border_latitudes
        self.border_longitudes = border_longitudes
        self.center_coordinate = center_coordinate


politician_elections_linker = db.Table(
    "politicians_elections_linker",
    db.Column(
        "election_id", db.Integer, db.ForeignKey("election.id"), primary_key=True
    ),
    db.Column(
        "politician_id", db.Integer, db.ForeignKey("politician.id"), primary_key=True
    ),
)


class Politician(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    party = db.Column(db.String())
    img_url = db.Column(db.String())
    incumbent = db.Column(db.Boolean)
    twitter = db.Column(db.String())
    facebook = db.Column(db.String())
    gov_level = db.Column(db.String())
    years_in_office = db.Column(db.Integer)
    last_election = db.Column(db.String())
    district_id = db.Column(
        db.Integer, db.ForeignKey("district.id")
    )  # handles the many to one relationship
    district_num = db.Column(db.Integer)
    elections = db.relationship(
        "Election",
        secondary=politician_elections_linker,
        lazy=False,
        backref=db.backref("politicians", lazy=False),
    )  # handles the many to many relationship

    def __init__(
        self,
        name,
        party,
        img_url,
        incumbent,
        twitter,
        facebook,
        gov_level,
        years_in_office,
        last_election,
        district_num,
    ):
        self.name = name
        self.party = party
        self.img_url = img_url
        self.incumbent = incumbent
        self.twitter = twitter
        self.facebook = facebook
        self.gov_level = gov_level
        self.years_in_office = years_in_office
        self.last_election = last_election
        self.district_num = district_num


class Election(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    gov_level = db.Column(db.String())
    num_votes = db.Column(db.Integer)
    general_date = db.Column(db.String())
    year = db.Column(db.Integer)
    winning_party = db.Column(db.String())
    winning_candidate = db.Column(db.String())
    # politicians should already be here from the backref in the politicians model
    youtube_vid = db.Column(db.String())
    district_id = db.Column(
        db.Integer, db.ForeignKey("district.id")
    )  # handles the many to one relationship
    district_num = db.Column(db.Integer)

    def __init__(
        self,
        name,
        gov_level,
        num_votes,
        general_date,
        year,
        winning_party,
        winning_candidate,
        youtube_vid,
        district_num,
    ):
        self.name = name
        self.gov_level = gov_level
        self.num_votes = num_votes
        self.general_date = general_date
        self.year = year
        self.winning_party = winning_party
        self.winning_candidate = winning_candidate
        self.youtube_vid = youtube_vid
        self.district_num = district_num


class BaseSchema(ma.Schema):
    SKIP_VALUES = [None]

    @post_dump
    def remove_skip_values(self, data, **kargs):
        return {
            key: value for key, value in data.items() if value not in self.SKIP_VALUES
        }


class PoliticianSchema(BaseSchema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    district = fields.Nested(
        "DistrictSchema",
        only=("id", "name", "number", "population"),
        attribute="district",
    )
    elections = fields.Nested(
        "ElectionSchema", only=("id", "name"), many=True, attribute="elections"
    )
    party = fields.Str(required=True)
    img_url = fields.Str(required=True)
    incumbent = fields.Bool(required=True)
    twitter = fields.Str(required=True)
    facebook = fields.Str(required=True)
    gov_level = fields.Str(required=True)
    years_in_office = fields.Int(required=True)
    last_election = fields.Str(required=True)


class DistrictSchema(BaseSchema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    type = fields.Str(required=True)
    number = fields.Int(required=True)
    politicians = fields.Nested(
        "PoliticianSchema", only=("id", "name", "party", "img_url"), many=True
    )
    incumbent_name = fields.Str(required=True)
    incumbent_party = fields.Str(required=True)
    incumbent_pic = fields.Str(required=True)
    population = fields.Int(required=True)
    elections = fields.Nested(
        "ElectionSchema", only=("id", "name"), many=True, attribute="elections"
    )
    male = fields.Float(required=True)
    female = fields.Float(required=True)
    race_white = fields.Float(required=True)
    race_black = fields.Float(required=True)
    race_asian = fields.Float(required=True)
    race_native = fields.Float(required=True)
    race_pacific_island = fields.Float(required=True)
    majority_race = fields.Str(required=True)
    ethnicity_hispanic = fields.Float(required=True)
    median_income = fields.Int(required=True)
    hs_grad = fields.Float(required=True)
    college_grad = fields.Float(required=True)
    # map_geoJSON = fields.Str(required=True)
    # border_latitudes = fields.List(fields.Float())
    # border_longitudes = fields.List(fields.Float())
    min_latitude = fields.Float(required=True)
    max_latitude = fields.Float(required=True)
    min_longitude = fields.Float(required=True)
    max_longitude = fields.Float(required=True)
    center_coordinate = fields.List(fields.Float())


class ElectionSchema(BaseSchema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    district = fields.Nested(
        "DistrictSchema",
        only=("id", "name", "number", "population"),
        attribute="district",
    )
    gov_level = fields.Str(required=True)
    num_votes = fields.Int(required=True)
    general_date = fields.Str(required=True)
    winning_party = fields.Str(required=True)
    winning_candidate = fields.Str(required=True)
    politicians = fields.Nested(
        "PoliticianSchema", only=("id", "name", "party", "img_url"), many=True
    )
    youtube_vid = fields.Str(required=True)


politician_schema = PoliticianSchema(many=True)
district_schema = DistrictSchema(many=True)
election_schema = ElectionSchema(many=True)

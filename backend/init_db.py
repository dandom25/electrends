from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
from dotenv import load_dotenv
import os
import urllib
import json


def init_db(app):
    load_dotenv()
    # Schema: "postgres+psycopg2://<USERNAME>:<PASSWORD>@<IP_ADDRESS>:<PORT>/<DATABASE_NAME>"
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = "postgresql+psycopg2://electrends_swe:SWE2022grp8@electrends.ccn2ykvna5vh.us-east-1.rds.amazonaws.com:5432/postgres"  # TODO change to using env var
    return SQLAlchemy(app)


# app.debug = True
# app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


def reset_db(db):
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("Database reset")

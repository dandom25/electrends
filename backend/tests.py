from unittest import main, TestCase
import requests


class Tests(TestCase):
    # Politicians
    def test_politicians_all(self):
        politicians = requests.get("https://api.electrends.me/politicians")
        assert politicians.status_code == 200
        data = politicians.json()
        assert (data["meta"]["total_count"]) == 744
        assert data["politicians"][0] == {
            "district": {
                "id": 2,
                "name": "Texas State Senate District 2",
                "number": 2,
                "population": 944576,
            },
            "elections": [
                {
                    "id": 112,
                    "name": "Texas State Senate District 2 General Election 2018",
                }
            ],
            "facebook": "https://www.facebook.com/KendallScudder",
            "gov_level": "Texas State Senate",
            "id": 1,
            "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Kendall_Scudder.jpg",
            "incumbent": False,  # maybe change to FALSE
            "last_election": "March 1, 2022",
            "name": "Kendall Scudder",
            "party": "Democratic",
            "twitter": "https://www.twitter.com/KendallScudder",
            "years_in_office": 0,
        }

    def test_politician_instance(self):
        politicians = requests.get("https://api.electrends.me/politicians/369")
        assert politicians.status_code == 200
        data = politicians.json()
        assert data == {
            "district": {
                "id": 57,
                "name": "Texas House of Representatives District 26",
                "number": 26,
                "population": 186766,
            },
            "elections": [
                {
                    "id": 349,
                    "name": "Texas House of Representatives District 26 General Election 2020",
                },
                {
                    "id": 499,
                    "name": "Texas House of Representatives District 26 General Election 2022",
                },
            ],
            "facebook": "https://www.facebook.com/teamjetton",
            "gov_level": "Texas House of Representatives",
            "id": 369,
            "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Jacey-Jetton.jpg",
            "incumbent": True,
            "last_election": "November 3, 2020",
            "name": "Jacey Jetton",
            "party": "Republican",
            "twitter": "https://www.twitter.com/JaceyJetton",
            "years_in_office": 1,
        }

    def test_politician_error(self):
        politicians = requests.get("https://api.electrends.me/politicians/800")
        assert politicians.status_code == 404
        data = politicians.json()
        assert data == {"error": "800 not found"}

    def test_politician_sort(self):
        politicians = requests.get("https://api.electrends.me/politicians?sort=name")
        assert politicians.status_code == 200
        data = politicians.json()
        assert (data["meta"]["total_count"]) == 744
        assert data["politicians"][0] == {
            "district": {
                "id": 180,
                "name": "Texas House of Representatives District 149",
                "number": 149,
                "population": 183586,
            },
            "elections": [
                {
                    "id": 322,
                    "name": "Texas House of Representatives District 149 General Election 2018",
                }
            ],
            "facebook": "https://www.facebook.com/aaronclosetexas/",
            "gov_level": "Texas House of Representatives",
            "id": 351,
            "img_url": "https://cdn.ballotpedia.org/images/thumb/f/fb/Silhouette_Placeholder_Image.png/150px-Silhouette_Placeholder_Image.png",
            "incumbent": False,
            "last_election": "November 6, 2018",
            "name": "Aaron Close",
            "party": "Libertarian",
            "twitter": "https://twitter.com/LPTexas",
            "years_in_office": 0,
        }

    def test_politician_filter(self):
        politicians = requests.get(
            "https://api.electrends.me/politicians?gov_level=Texas%20Congressional%20District"
        )
        assert politicians.status_code == 200
        data = politicians.json()
        assert (data["meta"]["total_count"]) == 195
        assert data["politicians"][0] == {
            "district": {
                "id": 185,
                "name": "Texas Congressional District 4",
                "number": 4,
                "population": 787256,
            },
            "elections": [
                {
                    "id": 40,
                    "name": "Texas Congressional District 4 General Election 2020",
                },
                {
                    "id": 76,
                    "name": "Texas Congressional District 4 General Election 2022",
                },
                {
                    "id": 125,
                    "name": "Texas State Senate District 30 General Election 2018",
                },
            ],
            "facebook": "https://www.facebook.com/fallonfortexas",
            "gov_level": "Texas Congressional District",
            "id": 31,
            "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/PatFallon.PNG",
            "incumbent": True,
            "last_election": "November 3, 2020",
            "name": "Pat Fallon",
            "party": "Republican",
            "twitter": "https://www.twitter.com/FallonForTexas",
            "years_in_office": 1,
        }

    def test_politician_search(self):
        politicians = requests.get(
            "https://api.electrends.me/politicians?search=Democrat"
        )
        assert politicians.status_code == 200
        data = politicians.json()
        assert (data["meta"]["total_count"]) == 321
        assert data["politicians"][0] == {
            "district": {
                "id": 2,
                "name": "Texas State Senate District 2",
                "number": 2,
                "population": 944576,
            },
            "elections": [
                {
                    "id": 112,
                    "name": "Texas State Senate District 2 General Election 2018",
                }
            ],
            "facebook": "https://www.facebook.com/KendallScudder",
            "gov_level": "Texas State Senate",
            "id": 1,
            "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Kendall_Scudder.jpg",
            "incumbent": False,
            "last_election": "March 1, 2022",
            "name": "Kendall Scudder",
            "party": "Democratic",
            "twitter": "https://www.twitter.com/KendallScudder",
            "years_in_office": 0,
        }

    # Districts
    def test_districts_all(self):
        districts = requests.get("https://api.electrends.me/districts")
        assert districts.status_code == 200
        data = districts.json()
        assert (data["meta"]["total_count"]) == 219
        assert data["districts"][0] == {
            "center_coordinate": [-96.029954, 33.120116499999995],
            "college_grad": 22.4,
            "elections": [
                {
                    "id": 112,
                    "name": "Texas State Senate District 2 General Election 2018",
                },
                {
                    "id": 144,
                    "name": "Texas State Senate District 2 General Election 2022",
                },
            ],
            "ethnicity_hispanic": 32.8,
            "female": 50.6,
            "hs_grad": 81.8,
            "id": 2,
            "incumbent_name": "Bob Hall",
            "incumbent_party": "Republican",
            "incumbent_pic": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/200/Bob-Hall.jpg",
            "majority_race": "White",
            "male": 49.4,
            "max_latitude": -95.307716,
            "max_longitude": 33.885,
            "median_income": 61401,
            "min_latitude": -96.752192,
            "min_longitude": 32.355233,
            "name": "Texas State Senate District 2",
            "number": 2,
            "politicians": [
                {
                    "id": 1,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Kendall_Scudder.jpg",
                    "name": "Kendall Scudder",
                    "party": "Democratic",
                },
                {
                    "id": 2,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Bob-Hall.jpg",
                    "name": "Bob Hall",
                    "party": "Republican",
                },
                {
                    "id": 68,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/PrinceGiadolor.jpeg",
                    "name": "Prince Giadolor",
                    "party": "Democratic",
                },
            ],
            "population": 944576,
            "race_asian": 2.5,
            "race_black": 13.5,
            "race_native": 1.2,
            "race_pacific_island": 0.1,
            "race_white": 54.3,
            "type": "State Senate",
        }

    def test_districts_instance(self):
        politicians = requests.get("https://api.electrends.me/districts/106")
        assert politicians.status_code == 200
        data = politicians.json()
        assert data == {
            "center_coordinate": [-106.14825116725049, 31.694829367616798],
            "college_grad": 18.3,
            "elections": [
                {
                    "id": 248,
                    "name": "Texas House of Representatives District 75 General Election 2018",
                },
                {
                    "id": 398,
                    "name": "Texas House of Representatives District 75 General Election 2020",
                },
                {
                    "id": 548,
                    "name": "Texas House of Representatives District 75 General Election 2022",
                },
            ],
            "ethnicity_hispanic": 90.2,
            "female": 51.5,
            "hs_grad": 74.9,
            "id": 106,
            "incumbent_name": "Mary Gonzalez",
            "incumbent_party": "Democratic",
            "incumbent_pic": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/200/Mary_Gonzalez.jpg",
            "majority_race": "White",
            "male": 48.5,
            "max_latitude": -105.958548,
            "max_longitude": 32.0024123076916,
            "median_income": 51794,
            "min_latitude": -106.337954334501,
            "min_longitude": 31.387246427542,
            "name": "Texas House of Representatives District 75",
            "number": 75,
            "politicians": [
                {
                    "id": 212,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Mary_Gonzalez.jpg",
                    "name": "Mary Gonzalez",
                    "party": "Democratic",
                }
            ],
            "population": 226395,
            "race_asian": 0.7,
            "race_black": 2.3,
            "race_native": 1.4,
            "race_pacific_island": 0.2,
            "race_white": 32.5,
            "type": "House of Representatives",
        }

    def test_districts_error(self):
        districts = requests.get("https://api.electrends.me/districts/300")
        assert districts.status_code == 404
        data = districts.json()
        assert data == {"error": "300 not found"}

    def test_districts_sort(self):
        districts = requests.get("https://api.electrends.me/districts?sort=population")
        assert districts.status_code == 200
        data = districts.json()
        assert (data["meta"]["total_count"]) == 219
        assert data["districts"][0] == {
            "center_coordinate": [-106.3614078804195, 31.7443475],
            "college_grad": 14.3,
            "elections": [
                {
                    "id": 399,
                    "name": "Texas House of Representatives District 76 General Election 2020",
                },
                {
                    "id": 249,
                    "name": "Texas House of Representatives District 76 General Election 2018",
                },
                {
                    "id": 549,
                    "name": "Texas House of Representatives District 76 General Election 2022",
                },
            ],
            "ethnicity_hispanic": 89.1,
            "female": 51.3,
            "hs_grad": 69.7,
            "id": 107,
            "incumbent_name": "Claudia Ordaz Perez",
            "incumbent_party": "Democratic",
            "incumbent_pic": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/200/Claudia-Ordaz-Perez.jpg",
            "majority_race": "White",
            "male": 48.7,
            "max_latitude": -106.271649,
            "max_longitude": 31.825929,
            "median_income": 34567,
            "min_latitude": -106.451166760839,
            "min_longitude": 31.662766,
            "name": "Texas House of Representatives District 76",
            "number": 76,
            "politicians": [],
            "population": 146284,
            "race_asian": 0.8,
            "race_black": 2.2,
            "race_native": 1.2,
            "race_pacific_island": 0.2,
            "race_white": 37.3,
            "type": "House of Representatives",
        }

    def test_districts_filter(self):
        districts = requests.get(
            "https://api.electrends.me/districts?type=Congressional"
        )
        assert districts.status_code == 200
        data = districts.json()
        assert (data["meta"]["total_count"]) == 36
        assert data["districts"][0] == {
            "center_coordinate": [-95.493711, 33.249078499999996],
            "college_grad": 22.5,
            "elections": [
                {
                    "id": 4,
                    "name": "Texas Congressional District 4 General Election 2018",
                },
                {
                    "id": 40,
                    "name": "Texas Congressional District 4 General Election 2020",
                },
                {
                    "id": 76,
                    "name": "Texas Congressional District 4 General Election 2022",
                },
            ],
            "ethnicity_hispanic": 15.8,
            "female": 50.7,
            "hs_grad": 87.2,
            "id": 185,
            "incumbent_name": "Pat Fallon",
            "incumbent_party": "Republican",
            "incumbent_pic": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/200/PatFallon.PNG",
            "majority_race": "White",
            "male": 49.3,
            "max_latitude": -94.042719,
            "max_longitude": 33.962142,
            "median_income": 56807,
            "min_latitude": -96.944703,
            "min_longitude": 32.536015,
            "name": "Texas Congressional District 4",
            "number": 4,
            "politicians": [
                {
                    "id": 31,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/PatFallon.PNG",
                    "name": "Pat Fallon",
                    "party": "Republican",
                },
                {
                    "id": 553,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/John_Ratcliffe_official_congressional_photo.jpg",
                    "name": "John Ratcliffe",
                    "party": "Republican",
                },
                {
                    "id": 554,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Catherine_Krantz.jpg",
                    "name": "Catherine Krantz",
                    "party": "Democratic",
                },
                {
                    "id": 555,
                    "img_url": "https://cdn.ballotpedia.org/images/thumb/f/fb/Silhouette_Placeholder_Image.png/150px-Silhouette_Placeholder_Image.png",
                    "name": "Ken Ashby",
                    "party": "Libertarian",
                },
                {
                    "id": 648,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Russell-Foster.jpg",
                    "name": "Russell Foster",
                    "party": "Democratic",
                },
                {
                    "id": 649,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Lou_Antonelli.jpg",
                    "name": "Lou Antonelli",
                    "party": "Libertarian",
                },
                {
                    "id": 650,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Tracy_Jones1.jpg",
                    "name": "Tracy Jones",
                    "party": "Other",
                },
                {
                    "id": 712,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Iroghama-Omere.jpg",
                    "name": "Iroghama Omere",
                    "party": "Democratic",
                },
            ],
            "population": 787256,
            "race_asian": 1.4,
            "race_black": 10.3,
            "race_native": 1.1,
            "race_pacific_island": 0.1,
            "race_white": 70.7,
            "type": "Congressional",
        }

    def test_districts_search(self):
        districts = requests.get("https://api.electrends.me/districts?search=Democrat")
        assert districts.status_code == 200
        data = districts.json()
        assert (data["meta"]["total_count"]) == 91
        assert data["districts"][0] == {
            "center_coordinate": [-97.3020055, 32.755113],
            "college_grad": 31.9,
            "elections": [
                {
                    "id": 118,
                    "name": "Texas State Senate District 10 General Election 2018",
                },
                {
                    "id": 152,
                    "name": "Texas State Senate District 10 General Election 2022",
                },
            ],
            "ethnicity_hispanic": 32.2,
            "female": 51.0,
            "hs_grad": 84.2,
            "id": 10,
            "incumbent_name": "Beverly Powell",
            "incumbent_party": "Democratic",
            "incumbent_pic": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/200/Beverly_Powell.jpeg",
            "majority_race": "White",
            "male": 49.0,
            "max_latitude": -97.053429,
            "max_longitude": 32.96126,
            "median_income": 65263,
            "min_latitude": -97.550582,
            "min_longitude": 32.548966,
            "name": "Texas State Senate District 10",
            "number": 10,
            "politicians": [
                {
                    "id": 15,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Beverly_Powell.jpeg",
                    "name": "Beverly Powell",
                    "party": "Democratic",
                },
                {
                    "id": 16,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Konni_Burton.jpg",
                    "name": "Konni Burton",
                    "party": "Republican",
                },
                {
                    "id": 72,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Phil_King.jpg",
                    "name": "Phil King",
                    "party": "Republican",
                },
            ],
            "population": 945496,
            "race_asian": 4.7,
            "race_black": 19.8,
            "race_native": 0.8,
            "race_pacific_island": 0.1,
            "race_white": 46.9,
            "type": "State Senate",
        }

    # Elections
    def test_elections_all(self):
        elections = requests.get("https://api.electrends.me/elections")
        assert elections.status_code == 200
        data = elections.json()
        assert (data["meta"]["total_count"]) == 623
        assert data["elections"][0] == {
            "district": {
                "id": 182,
                "name": "Texas Congressional District 1",
                "number": 1,
                "population": 722363,
            },
            "general_date": "November 6, 2018",
            "gov_level": "Texas Congressional District",
            "id": 1,
            "name": "Texas Congressional District 1 General Election 2018",
            "num_votes": 232720,
            "politicians": [
                {
                    "id": 543,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/5c60cec4-7bcb-4db1-8582-b40b7832c36b.jpg",
                    "name": "Louis Gohmert",
                    "party": "Republican",
                },
                {
                    "id": 544,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Shirley_McKellar.jpg",
                    "name": "Shirley McKellar",
                    "party": "Democratic",
                },
                {
                    "id": 545,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/jeffcallawaycampaignheadshot.jpg",
                    "name": "Jeff Callaway",
                    "party": "Libertarian",
                },
            ],
            "winning_candidate": "Louis B. Gohmert Jr.",
            "winning_party": "Republican",
            "youtube_vid": "https://www.youtube.com/watch?v=NAtw5rWe3-w",
        }

    def test_elections_instance(self):
        elections = requests.get("https://api.electrends.me/elections/19")
        assert elections.status_code == 200
        data = elections.json()
        assert data == {
            "district": {
                "id": 200,
                "name": "Texas Congressional District 19",
                "number": 19,
                "population": 731424,
            },
            "general_date": "November 6, 2018",
            "gov_level": "Texas Congressional District",
            "id": 19,
            "name": "Texas Congressional District 19 General Election 2018",
            "num_votes": 201985,
            "politicians": [
                {
                    "id": 598,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Jodey_Arrington_115th_congress_photo.jpg",
                    "name": "Jodey Arrington",
                    "party": "Republican",
                },
                {
                    "id": 599,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Campaign_Headshot.jpg",
                    "name": "Miguel Levario",
                    "party": "Democratic",
                },
            ],
            "winning_candidate": "Jodey Arrington",
            "winning_party": "Republican",
            "youtube_vid": "https://www.youtube.com/watch?v=NAtw5rWe3-w",
        }

    def test_elections_error(self):
        elections = requests.get("https://api.electrends.me/elections/800")
        assert elections.status_code == 404
        data = elections.json()
        assert data == {"error": "800 not found"}

    def test_elections_sort(self):
        elections = requests.get(
            "https://api.electrends.me/elections?sort=district_num"
        )
        assert elections.status_code == 200
        data = elections.json()
        assert (data["meta"]["total_count"]) == 623
        assert data["elections"][0] == {
            "district": {
                "id": 219,
                "name": "Texas US Senate",
                "number": 0,
                "population": 27429639,
            },
            "general_date": "November 3, 2020",
            "gov_level": "US Senate",
            "id": 110,
            "name": "Texas US Senate General Election 2020",
            "num_votes": 11144040,
            "politicians": [
                {
                    "id": 631,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/MJ_Hegar_Headshot.jpg",
                    "name": "M.J. Hegar",
                    "party": "Democratic",
                },
                {
                    "id": 736,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/John_Cornyn.jpg",
                    "name": "John Cornyn",
                    "party": "Republican",
                },
                {
                    "id": 737,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Kerry-McKennon.jpg",
                    "name": "Kerry McKennon",
                    "party": "Libertarian",
                },
                {
                    "id": 738,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/DavidBCollins.jpeg",
                    "name": "David Collins",
                    "party": "Green",
                },
                {
                    "id": 739,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/RicardoTurullols-Bonilla.jpg",
                    "name": "Ricardo Turullols-Bonilla",
                    "party": "Other",
                },
            ],
            "winning_candidate": "John Cornyn",
            "winning_party": "Republican",
            "youtube_vid": "https://www.youtube.com/watch?v=uRu_JcarCDY",
        }

    def test_elections_filter(self):
        elections = requests.get(
            "https://api.electrends.me/elections?winning_party=Republican"
        )
        assert elections.status_code == 200
        data = elections.json()
        assert (data["meta"]["total_count"]) == 233
        assert data["elections"][0] == {
            "district": {
                "id": 182,
                "name": "Texas Congressional District 1",
                "number": 1,
                "population": 722363,
            },
            "general_date": "November 6, 2018",
            "gov_level": "Texas Congressional District",
            "id": 1,
            "name": "Texas Congressional District 1 General Election 2018",
            "num_votes": 232720,
            "politicians": [
                {
                    "id": 543,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/5c60cec4-7bcb-4db1-8582-b40b7832c36b.jpg",
                    "name": "Louis Gohmert",
                    "party": "Republican",
                },
                {
                    "id": 544,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Shirley_McKellar.jpg",
                    "name": "Shirley McKellar",
                    "party": "Democratic",
                },
                {
                    "id": 545,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/jeffcallawaycampaignheadshot.jpg",
                    "name": "Jeff Callaway",
                    "party": "Libertarian",
                },
            ],
            "winning_candidate": "Louis B. Gohmert Jr.",
            "winning_party": "Republican",
            "youtube_vid": "https://www.youtube.com/watch?v=NAtw5rWe3-w",
        }

    def test_elections_search(self):
        elections = requests.get("https://api.electrends.me/elections?search=Democrat")
        assert elections.status_code == 200
        data = elections.json()
        assert (data["meta"]["total_count"]) == 173
        assert data["elections"][0] == {
            "district": {
                "id": 211,
                "name": "Texas Congressional District 30",
                "number": 30,
                "population": 782976,
            },
            "general_date": "November 6, 2018",
            "gov_level": "Texas Congressional District",
            "id": 30,
            "name": "Texas Congressional District 30 General Election 2018",
            "num_votes": 183174,
            "politicians": [
                {
                    "id": 402,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Shawn_Jones.jpg",
                    "name": "Shawn Jones",
                    "party": "Libertarian",
                },
                {
                    "id": 629,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/Eddie_Bernice_Johnson.jpg",
                    "name": "Eddie Bernice Johnson",
                    "party": "Democratic",
                },
            ],
            "winning_candidate": "Eddie Bernice Johnson",
            "winning_party": "Democratic",
            "youtube_vid": "https://www.youtube.com/watch?v=NAtw5rWe3-w",
        }, {
            "district": {
                "id": 188,
                "name": "Texas Congressional District 7",
                "number": 7,
                "population": 800911,
            },
            "general_date": "November 6, 2018",
            "gov_level": "Texas Congressional District",
            "id": 7,
            "name": "Texas Congressional District 7 General Election 2018",
            "num_votes": 243601,
            "politicians": [
                {
                    "id": 561,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/LIZZIE_FLETCHER.jpg",
                    "name": "Lizzie Pannill Fletcher",
                    "party": "Democratic",
                },
                {
                    "id": 562,
                    "img_url": "https://s3.amazonaws.com/ballotpedia-api4/files/thumbs/200/300/John_Culberson.jpg",
                    "name": "John Culberson",
                    "party": "Republican",
                },
            ],
            "winning_candidate": "Lizzie Pannill Fletcher",
            "winning_party": "Democratic",
            "youtube_vid": "https://www.youtube.com/watch?v=NAtw5rWe3-w",
        }


if __name__ == "__main__":  # pragma: no cover
    main()

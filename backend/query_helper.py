def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None

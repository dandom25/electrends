from flask import jsonify

# from init_db import app, db
from setup_db import (
    District,
    Politician,
    Election,
    politician_schema,
    district_schema,
    election_schema,
    app,
    db,
)

from sqlalchemy import or_, asc, desc, cast
import sqlalchemy.sql.sqltypes as types
from query_helper import *
import flask
import json


politician_filter_args = ["district_num", "party", "incumbent", "gov_level"]
election_filter_args = ["gov_level", "year", "district_num", "winning_party"]
district_filter_args = ["type", "incumbent_party", "number", "majority_race"]

politician_search_args = [
    "name",
    "gov_level",
    "years_in_office",
    "party",
    "district_num",
]
election_search_args = [
    "name",
    "year",
    "gov_level",
    "winning_party",
    "winning_candidate",
]
district_search_args = [
    "name",
    "type",
    "number",
    "incumbent_name",
    "incumbent_party",
    "majority_race",
]


# https://betterprogramming.pub/how-to-implement-filtering-sorting-and-pagination-in-flask-c4def1ca004a
# https://gitlab.com/flight-right/cs373_project/-/blob/develop/backend/app.py
def filter_results(args, possible_params, curr_query, curr_model):
    for param in possible_params:
        if param in args:
            attr = cast(getattr(curr_model, param), types.String)
            values = []
            for val in args[param].split(","):
                values.append(attr.ilike(val))  # ilike() makes it case insensistive
            curr_query = curr_query.filter(or_(*values))
    return curr_query


def sort_results(args, curr_query, curr_model):
    if "sort" in args:
        attr = getattr(curr_model, args["sort"])
        if "desc" in args:
            curr_query = curr_query.order_by(attr.desc())
        else:
            curr_query = curr_query.order_by(attr)
    return curr_query


# similar to this https://stackoverflow.com/questions/61672739/try-to-create-a-search-using-flask-and-flask-sqlachemy
# but more efficient than listing out each column
# also similar to # https://gitlab.com/flight-right/cs373_project/-/blob/develop/backend/app.py
def search_results(args, curr_query, curr_model, search_attrs):
    if "search" in args:
        values = []
        for col in search_attrs:
            for param in args["search"].split(" "):
                curr_filter = cast(getattr(curr_model, col), types.String).ilike(
                    f"%{param}%"
                )
                values.append(curr_filter)
        curr_query = curr_query.filter(or_(*values))
    return curr_query


# following https://www.youtube.com/watch?v=WFzRy8KVcrM to implement pagination on backend to handle our large amounts of data

# Politicians
@app.route("/politicians", methods=["GET"])
def all_politicians():
    page_num = flask.request.args.get("page_num", 1, type=int)
    per_page = flask.request.args.get("per_page", 18, type=int)

    pol_query = db.session.query(Politician)
    pol_query = filter_results(
        flask.request.args.to_dict(), politician_filter_args, pol_query, Politician
    )
    pol_query = search_results(
        flask.request.args.to_dict(), pol_query, Politician, politician_search_args
    )
    pol_query = sort_results(flask.request.args.to_dict(), pol_query, Politician)

    pol_page = pol_query.paginate(page=page_num, per_page=per_page)
    output = politician_schema.dump(pol_page.items, many=True)

    meta = {
        "page": pol_page.page,
        "num_responses": len(output),
        "pages": pol_page.pages,
        "total_count": pol_page.total,
        "prev_page": pol_page.prev_num,
        "next_page": pol_page.next_num,
        "has_next": pol_page.has_next,
        "has_prev": pol_page.has_prev,
    }

    return jsonify({"politicians": output, "meta": meta})


@app.route("/politicians/<int:id>", methods=["GET"])
def single_politician(id):
    if id > 744 or id < 0:
        response = flask.Response(
            json.dumps({"error": str(id) + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    politician = db.session.query(Politician).filter_by(id=id)
    politician = politician_schema.dump(politician, many=True)[0]

    return politician


# Elections
@app.route("/elections", methods=["GET"])
def all_elections():
    page_num = flask.request.args.get("page_num", 1, type=int)
    per_page = flask.request.args.get("per_page", 18, type=int)

    elec_query = db.session.query(Election)
    elec_query = filter_results(
        flask.request.args.to_dict(), election_filter_args, elec_query, Election
    )
    elec_query = search_results(
        flask.request.args.to_dict(), elec_query, Election, election_search_args
    )
    elec_query = sort_results(flask.request.args.to_dict(), elec_query, Election)

    elec_page = elec_query.paginate(page=page_num, per_page=per_page)
    output = election_schema.dump(elec_page.items, many=True)

    meta = {
        "page": elec_page.page,
        "num_responses": len(output),
        "pages": elec_page.pages,
        "total_count": elec_page.total,
        "prev_page": elec_page.prev_num,
        "next_page": elec_page.next_num,
        "has_next": elec_page.has_next,
        "has_prev": elec_page.has_prev,
    }

    return jsonify({"elections": output, "meta": meta})


@app.route("/elections/<int:id>", methods=["GET"])
def single_election(id):
    if id > 623 or id < 0:
        response = flask.Response(
            json.dumps({"error": str(id) + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    election = db.session.query(Election).filter_by(id=id)
    election = election_schema.dump(election, many=True)[0]
    return election


# Districts
@app.route("/districts", methods=["GET"])
def all_districts():
    page_num = flask.request.args.get("page_num", 1, type=int)
    per_page = flask.request.args.get("per_page", 18, type=int)

    dist_query = db.session.query(District)
    dist_query = filter_results(
        flask.request.args.to_dict(), district_filter_args, dist_query, District
    )
    dist_query = search_results(
        flask.request.args.to_dict(), dist_query, District, district_search_args
    )
    dist_query = sort_results(flask.request.args.to_dict(), dist_query, District)

    dist_page = dist_query.paginate(page=page_num, per_page=per_page)
    output = district_schema.dump(dist_page.items, many=True)

    meta = {
        "page": dist_page.page,
        "num_responses": len(output),
        "pages": dist_page.pages,
        "total_count": dist_page.total,
        "prev_page": dist_page.prev_num,
        "next_page": dist_page.next_num,
        "has_next": dist_page.has_next,
        "has_prev": dist_page.has_prev,
    }

    return jsonify({"districts": output, "meta": meta})


@app.route("/districts/<int:id>", methods=["GET"])
def single_district(id):
    if id > 219 or id < 0:
        response = flask.Response(
            json.dumps({"error": str(id) + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    district = db.session.query(District).filter_by(id=id)
    district = district_schema.dump(district, many=True)[0]
    return district


# example from skeleton
@app.route("/")
def hello_world():
    return "Welcome to the Electrends API!"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)

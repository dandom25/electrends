from init_db import reset_db
from setup_db import db, Politician, District, Election
import pandas as pd
import numpy as np


def load_districts(file_name):
    with open(file_name) as f:
        # read csv
        df = pd.read_csv(file_name)
        df = df.replace(np.nan, None, regex=True)
        for _, row in df.iterrows():
            add_district(row)


def load_elections(file_name):
    with open(file_name) as f:
        # read csv
        df = pd.read_csv(file_name)
        df = df.replace(np.nan, None, regex=True)
        for _, row in df.iterrows():
            add_election(row)


def load_politicians(file_name):
    with open(file_name) as f:
        # read csv
        df = pd.read_csv(file_name)
        df = df.replace(np.nan, None, regex=True)
        for _, row in df.iterrows():
            add_politician(row)


# using this to query https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/ for links
def add_district(inp):
    curr_district = {}
    curr_district["name"] = inp["name"]
    curr_district["type"] = inp["type"]
    curr_district["number"] = inp["number"]
    curr_district["incumbent_name"] = inp["incumbent_name"]
    curr_district["incumbent_party"] = inp["incumbent_party"]
    curr_district["incumbent_pic"] = inp["incumbent_pic"]
    if not curr_district["incumbent_pic"]:
        curr_district[
            "incumbent_pic"
        ] = "https://www.kindpng.com/picc/m/24-248253_user-profile-default-image-png-clipart-png-download.png"

    curr_district["population"] = inp["pop"]
    curr_district["male"] = inp["male"]
    curr_district["female"] = inp["female"]
    curr_district["race_white"] = inp["race_white"]
    curr_district["race_black"] = inp["race_black"]
    curr_district["race_asian"] = inp["race_asian"]
    curr_district["race_native"] = inp["race_native"]
    curr_district["race_pacific_island"] = inp["race_pacific_island"]
    curr_district["median_income"] = inp["med_income"]
    curr_district["hs_grad"] = inp["hs_grad"]
    curr_district["college_grad"] = inp["college_grad"]
    curr_district["male"] = inp["male"]
    curr_district["female"] = inp["female"]
    curr_district["majority_race"] = inp["majority_race"]
    curr_district["ethnicity_hispanic"] = inp["ethnicity_hispanic"]
    curr_district["hs_grad"] = inp["hs_grad"]
    curr_district["college_grad"] = inp["college_grad"]
    curr_district["map_geoJSON"] = inp["map_geoJSON"]
    curr_district["min_latitude"] = inp["min_lat"]
    curr_district["max_latitude"] = inp["max_lat"]
    curr_district["min_longitude"] = inp["min_long"]
    curr_district["max_longitude"] = inp["max_long"]

    lat_list = inp["lat_list"]
    curr_district["border_latitudes"] = []
    if lat_list:
        lat_list = lat_list[1:-1].split(",")
        for lat in lat_list:
            curr_district["border_latitudes"].append(float(lat.strip()))

    long_list = inp["long_list"]
    curr_district["border_longitudes"] = []
    if long_list:
        long_list = long_list[1:-1].split(",")
        for long in long_list:
            curr_district["border_longitudes"].append(float(long.strip()))

    center = inp["map_center"]
    center = center[1:-1].split(", ")
    curr_district["center_coordinate"] = [float(center[0]), float(center[1])]

    db.session.add(
        District(
            curr_district["name"],
            curr_district["type"],
            curr_district["number"],
            curr_district["incumbent_name"],
            curr_district["incumbent_party"],
            curr_district["incumbent_pic"],
            curr_district["population"],
            curr_district["race_white"],
            curr_district["race_black"],
            curr_district["race_asian"],
            curr_district["race_native"],
            curr_district["race_pacific_island"],
            curr_district["median_income"],
            curr_district["hs_grad"],
            curr_district["college_grad"],
            curr_district["male"],
            curr_district["female"],
            curr_district["majority_race"],
            curr_district["ethnicity_hispanic"],
            curr_district["map_geoJSON"],
            curr_district["min_latitude"],
            curr_district["max_latitude"],
            curr_district["min_longitude"],
            curr_district["max_longitude"],
            curr_district["border_latitudes"],
            curr_district["border_longitudes"],
            curr_district["center_coordinate"],
        )
    )


def add_election(inp):
    curr_election = {}
    curr_election["name"] = inp["name"]
    curr_election["gov_level"] = inp["gov_lvl"]
    curr_election["num_votes"] = inp["num_votes"]
    curr_election["general_date"] = inp["general_date"].strip()
    curr_election["year"] = int(inp["general_date"].strip().split(" ")[-1])
    party_conv = {
        "(D)": "Democratic",
        "(R)": "Republican",
        "Other": "Other",
        "TBD": "TBD",
    }
    curr_election["winning_party"] = party_conv[inp["party_elec"]]
    curr_election["winning_candidate"] = inp["candidate_elec"]

    if curr_election["gov_level"] == "Texas Congressional District":
        curr_election["youtube_vid"] = "https://www.youtube.com/watch?v=NAtw5rWe3-w"
    if curr_election["gov_level"] == "US Senate":
        curr_election["youtube_vid"] = "https://www.youtube.com/watch?v=uRu_JcarCDY"
    if curr_election["gov_level"] == "US Presidency":
        curr_election["youtube_vid"] = "https://www.youtube.com/watch?v=Jdadb7qMBcE"
    if curr_election["gov_level"] == "Texas Senate":
        curr_election["youtube_vid"] = "https://www.youtube.com/watch?v=2K_-cP07iAQ"
    if curr_election["gov_level"] == "Texas House of Representatives":
        curr_election["youtube_vid"] = "https://www.youtube.com/watch?v=xZnE16cDxXI"

    curr_election["district_num"] = 0
    temp = inp["dist_name"].split(" ")[-1]
    if temp.isdigit():
        curr_election["district_num"] = int(temp)

    new_election = Election(
        curr_election["name"],
        curr_election["gov_level"],
        curr_election["num_votes"],
        curr_election["general_date"],
        curr_election["year"],
        curr_election["winning_party"],
        curr_election["winning_candidate"],
        curr_election["youtube_vid"],
        curr_election["district_num"],
    )
    new_election.district = (
        db.session.query(District).filter_by(name=inp["dist_name"]).first()
    )

    db.session.add(new_election)


# using this to query https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/ for links
def add_politician(inp):
    curr_politician = {}
    curr_politician["name"] = inp["name"]
    curr_politician["party"] = inp["party"]

    curr_politician["img_url"] = inp["image_url"]
    if not curr_politician["img_url"]:
        curr_politician[
            "img_url"
        ] = "https://www.kindpng.com/picc/m/24-248253_user-profile-default-image-png-clipart-png-download.png"

    curr_politician["incumbent"] = inp["incumbent"]
    if not curr_politician["incumbent"]:
        curr_politician["incumbent"] = False

    curr_politician["twitter"] = inp["twitter"]
    if not curr_politician["twitter"]:
        if inp["party"] == "Democratic":
            curr_politician["twitter"] = "https://twitter.com/texasdemocrats"
        elif inp["party"] == "Republican":
            curr_politician["twitter"] = "https://twitter.com/TexasGOP"
        elif inp["party"] == "Libertarian":
            curr_politician["twitter"] = "https://twitter.com/LPTexas"
        else:
            curr_politician["twitter"] = "https://twitter.com/texasgov"

    curr_politician["facebook"] = inp["facebook"]
    if not curr_politician["facebook"]:
        if inp["party"] == "Democratic":
            curr_politician[
                "facebook"
            ] = "https://www.facebook.com/TexasDemocraticParty"
        elif inp["party"] == "Republican":
            curr_politician["facebook"] = "https://www.facebook.com/TexasGOP"
        elif inp["party"] == "Libertarian":
            curr_politician["facebook"] = "https://www.facebook.com/libertarians"
        else:
            curr_politician["facebook"] = "https://www.facebook.com/Texas.gov"

    curr_politician["gov_level"] = inp["gov_lvl"]

    curr_politician["years_in_office"] = inp["year_in_pos"]
    if not curr_politician["years_in_office"]:
        curr_politician["years_in_office"] = 0

    curr_politician["last_election"] = inp["last_election"]
    if not curr_politician["last_election"]:
        curr_politician["last_election"] = "N/A"

    curr_politician["district_num"] = 0
    temp = inp["district"].split(" ")[-1]
    if temp.isdigit():
        curr_politician["district_num"] = int(temp)

    new_politician = Politician(
        curr_politician["name"],
        curr_politician["party"],
        curr_politician["img_url"],
        curr_politician["incumbent"],
        curr_politician["twitter"],
        curr_politician["facebook"],
        curr_politician["gov_level"],
        curr_politician["years_in_office"],
        curr_politician["last_election"],
        curr_politician["district_num"],
    )
    new_politician.district = (
        db.session.query(District).filter_by(name=inp["district"]).first()
    )
    elections = inp["elections"]
    elections = elections[1:-1].split(",")
    for var in elections:
        elec = var.strip()[1:-1]
        new_elec = db.session.query(Election).filter_by(name=elec).first()
        if new_elec is not None:
            new_politician.elections.append((new_elec))

    db.session.add(new_politician)


def upload_data():
    district_csv = "data/district_data.csv"
    politician_csv = "data/politician_data.csv"
    election_csv = "data/election_data.csv"

    print("pushing districs")
    load_districts(district_csv)
    db.session.commit()
    print("pushing elections")
    load_elections(election_csv)
    db.session.commit()
    print("pushing politicians")
    load_politicians(politician_csv)
    db.session.commit()
    print("Done populating database!")


reset_db(db)
upload_data()
